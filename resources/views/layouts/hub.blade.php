<!DOCTYPE html>
<html lang="en" class="full-height">
    <head>
        @section('meta')
            @include('partials.meta')
        @show
    </head>

    <body class="app full-height">
        <div class="container-fluid full-height">
            <div class="row topbar-nav">

                    @section('topbar')
                        @include('modules.topbar')
                    @show

            </div>
            <div class="row full-height">
                <div class="p-l-0 full-height fixed-sidebar">
                    @section('nav')
                        @include('modules.nav')
                    @show
                </div>
                <div class="fixed-content">
                    @if(Session::has('error') || Session::has('success'))
                        @if(Session::has('error'))
                            <div class="alert alert-danger" id="flash-alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success" id="flash-alert">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                    @endif
                    @yield('body')
                </div>
            </div>
        </div>

        @section('footer')
            @include('partials.footer')
        @show
        <script src="/js/hub.js"></script>
    </body>
</html>
