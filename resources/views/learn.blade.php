@extends('layouts.app')

@section('body')
    <div class="container-fluid">
        <div class="row">
            @foreach($subjects as $subject)
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <a href="/learn/{{ strtolower($subject->name) }}" class="pane btn-block">
                        <div class="pane-image">
                            <img src="{{ $subject->img }}" alt="">
                        </div>
                        <div class="pane-text">
                            <div class="pane-text-main">{{ $subject->name }}</div>
                            <div class="pane-text-sub">View Courses</div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@stop
