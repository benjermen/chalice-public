@extends('layouts.app')

@section('body')
    <div class="col-md-6">
        <h2>Create an Assignment</h2>
        <br>

        <form action="/create/assignment" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="class_select">Apply to classroom:</label>
                <select name="classroom" id="class_select" class="form-control">
                    @if(count($classrooms) > 0)
                        @foreach($classrooms as $classroom)
                            <option value="{{ $classroom->id }}" @if(Request::old('classroom_select') == $classroom->id) selected @endif>{{ $classroom->name }}</option>
                        @endforeach
                    @else
                        <option value="0">You have no classrooms.</option>
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="assign-name">Name</label>
                <input type="text" class="form-control" id="assign-name" name="name" placeholder="Chapter 1 Quiz...">
            </div>

            <div class="form-group">
                <label for="assign-description">Description</label>
                <textarea name="description" id="assign-description" cols="30" rows="10" class="form-control">{{ Request::old('assignment_description') }}</textarea>
            </div>

            <br>
            <div class="text-center"><h4>--- Content ---</h4></div>
            <br>

            <div class="form-group">
                <label for="test-select">Make it a test:</label>
                <select name="test" id="test-select" class="form-control">
                    @if(count($tests) > 0)
                        <option value="">No test, file only.</option>
                        @foreach($tests as $test)
                            <option value="{{ $test->id }}" @if(Request::old('test_select') == $test->id) selected @endif>{{ $test->name }}</option>
                        @endforeach
                    @else
                        <option value="" disabled>You have no tests.</option>
                    @endif
                </select>
            </div>

            <div class="form-group">
                <label for="file-upload" class="full-width">OR Add a file for students to download</label>
                <input type="file" name="file" class="m-b-1" id="file-upload">
            </div>

            @if(Session::has('testError'))
                <div class="form-group">
                    <div class="alert alert-danger">
                        {{ Session::get('testError') }}
                    </div>
                </div>
            @endif

            @if(strlen($errors->assignment->first()) > 0)
                <div class="alert alert-danger">
                    {{ $errors->assignment->first() }}
                </div>
            @endif

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Create Assignment!">
            </div>
        </form>

        @if(Session::has('success'))
            <div class="form-group">
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
    </div>

@stop