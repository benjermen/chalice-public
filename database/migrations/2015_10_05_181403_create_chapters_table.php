<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function(Blueprint $table){
            $table->increments('id');
            $table->integer('set_id')->nullable();
            $table->integer('course_id')->unsigned()->nullable();
            $table->integer('classroom_id')->unsigned()->nullable();
            $table->string('description');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('chapters');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
