var socket = io('http://localhost:3000');
var notifCount = $('.badge .notifcount');
var notifCountWrapper = $('#notif-count');

function hideAns(checkbox)
{
    $('#ans').toggleClass("hidden");
    $('#c-d').toggleClass("hidden");
    $('.t-f').toggleClass("col-xs-3").toggleClass("col-xs-6");
}

function hasFromUser(notification)
{
    if(notification.from_user_id != null)
    {
        return '<a href="/user/' + notification.from_user_id + '"><img src="/images/user/' + notification.from_user_id + '" alt="" class="notif-img"> </a>';
    }
}

function isFriendRequest(notification)
{
    if(notification.purpose == 1)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptfriendrequest/' + notification.from_user_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declinefriendrequest/' + notification.from_user_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function isClassInvite(notification)
{
    if(notification.purpose == 11)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptclassinvite/' + notification.from_user_id + '/' + notification.opt_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declineclassinvite/' +  notification.from_user_id + '/' + notification.opt_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function isClassRequest(notification)
{
    if(notification.purpose == 12)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptclassrequest/' + notification.from_user_id + '/' + notification.opt_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declineclassrequest/' +  notification.from_user_id + '/' + notification.opt_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function csrf()
{
    return '<input type="hidden" name="_token" value="' + $('meta[name="_token"]').attr('content') + '">';
}

function user_channel()
{
    return $('meta[name="channel"]').attr('content');
}


socket.on("notifAction:App\\Events\\NotificationCreated:" + user_channel(), function (notification) {

    if(!notifCount.val())
    {
        notifCountWrapper.html('<span class="badge notifcount">1</span>');
    }
    else
    {
        var count = parseInt(notifCount.text());
        count++;
        notifCount.text(count);
    }

    notifCount.addClass('animated bounce');
    setTimeout(function() {
        notifCount.removeClass('animated bounce');
    }, 1200);

    if(notification.purpose == 1)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isFriendRequest(notification) + '</div> </a> </li>');
    }

    if(notification.purpose == 11)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isClassInvite(notification) + '</div> </a> </li>');
    }

    if(notification.purpose == 12)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isClassRequest(notification) + '</div> </a> </li>');
    }
});