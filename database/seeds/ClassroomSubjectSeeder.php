<?php

use Illuminate\Database\Seeder;
use App\Subject as Subject;
use App\Classroom as Classroom;

class ClassroomSubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->generateClassroomSubjects();
    }

    public function generateClassroomSubjects()
    {
        $classroomids = Classroom::lists('id')->toArray();
        $subjectids = Subject::lists('id')->toArray();

        foreach($classroomids as $classroom)
        {
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
            DB::statement('insert into classroom_subject values (?,?)', [$classroom, Faker\Factory::create()->randomElement($subjectids)]);
        }
    }
}
