<?php

use Illuminate\Support\Facades\Auth as Auth;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* AUTHED ROUTES */
Route::group(['middleware' => 'auth'], function () {

    //GETS (PAGE SERVES)
    Route::get('/profileSetup', 'ProfileSetupController@index');
    Route::get('/hub', 'HubController@index');
    Route::get('/settings', 'SettingsController@index');
    Route::get('/mycourses', 'MyCoursesController@index');
    Route::get('/messages', 'ConversationController@index');
    Route::get('/friends', 'FriendsController@index');
    Route::get('/gradebook', 'GradesController@index');
    Route::get('/createthread/{id}', 'ThreadController@create');
    Route::get('/queries/users/', 'ProfileController@query');
    Route::get('/queries/classes/', 'ClassroomController@query');
    Route::get('/classroom/{id}', 'ClassroomController@show');
    Route::get('/assignment/{id}', 'AssignmentController@show');
    Route::get('/assignment/{id}/file', 'AssignmentController@downloadFile');
    Route::get('/attempt/{testid}/assignment/{assignmentid}', 'AttemptController@newAttempt');
    Route::get('/attempt/{attemptid}/question/{questionid}', 'AttemptController@viewQuestion');
    Route::get('/gradebook/class/{userid}/{classroomid}', 'GradesController@showClassroomGrades');
    Route::get('/chapterset/{id}', 'ChapterSetController@show');

    //POSTS
    Route::post('createconversation', 'ConversationController@create');
    Route::post('sendfriendrequest/{touser}', 'FriendsController@sendRequest');
    Route::post('acceptfriendrequest/{fromuser}', 'FriendsController@acceptRequest');
    Route::post('declinefriendrequest/{fromuser}', 'FriendsController@declineRequest');
    Route::post('/settings/resetPassword', 'ProfileController@updatePassword');
    Route::post('/settings/changeEmail', 'ProfileController@updateEmail');
    Route::post('/settings/image', 'ProfileController@image');
    Route::post('/createthread', 'ThreadController@store');
    Route::post('/messages/send/{id}', 'MessageController@send');
    Route::post('/user/updateheader/{id}', 'ProfileController@updateHeaderImage');
    Route::post('/attempt/{attemptid}/question/{questionid}/skip', 'AttemptController@skipQuestion');
    Route::post('/attempt/{attemptid}/question/{questionid}', 'AttemptController@answerQuestion');
    Route::post('/autograde/attempt/{attemptid}', 'AttemptController@autoGrade');
    Route::post('/addcomment/{lessonid}', 'LessonController@addComment');
    Route::post('/acceptclassinvite/{fromuser}/{classroomid}', 'ClassroomController@acceptInvite');
    Route::post('/declineclassinvite/{fromuser}/{classroomid}', 'ClassroomController@declineInvite');
    Route::post('/classroom/requestaccess', 'ClassroomController@requestAccess');

    //MODIFIERS
    Route::delete('removefriend/{friendid}', 'FriendsController@destroy');
    Route::delete('/conversation/delete/{id}', 'ConversationController@destroy');

    /* TEACHER ONLY ROUTES */
    Route::group(['middleware' => 'teacher'], function () {
        //GETS (PAGE SERVES)
        Route::get('/create', function () {
            return view('create');
        });
        Route::get('/create/classroom', 'ClassroomController@create');
        Route::get('/create/chapter', 'ChapterController@create');
        Route::get('/create/test', 'TestController@create');
        Route::get('/create/assignment', 'AssignmentController@create');
        Route::get('/classroom/{id}/details', 'ClassroomController@details');
        Route::get('/classroom/{id}/addstudent', 'ClassroomController@addStudent');
        Route::get('/assignment/{id}/delete', 'AssignmentController@destroy');
        Route::get('/chapter/{id}/delete', 'ChapterController@destroy');
        Route::get('/test/{id}', 'TestController@show');
        Route::get('/test/{id}/edit', 'TestController@edit');
        Route::get('/test/{id}/edit/addquestion', 'TestController@questionAdd');
        Route::get('/test/{testid}/edit/question/{questionid}', 'TestController@questionEdit');
        Route::get('test/{testid}/delete/question/{questionid}', 'QuestionController@destroy');
        Route::get('/gradebook/classroom/{userid}/{classroomid}', 'GradesController@showClassroomGrades');
        Route::get('/manage', 'ManageController@index');
        Route::get('/create/chapterset', 'ChapterSetController@create');
        Route::get('/manage/archives', 'ManageController@archives');
        Route::delete('thread/{threadid}/delete', 'ThreadController@destroy');

        //POSTS
        Route::post('/create/classroom', 'ClassroomController@store');
        Route::post('/create/lesson', 'LessonController@store');
        Route::post('/create/test', 'TestController@store');
        Route::post('/create/assignment', 'AssignmentController@store');
        Route::post('/create/lesson', 'LessonController@store');
        Route::post('/classroom/updateheader/{id}', 'ClassroomController@updateHeaderImage');
        Route::post('/create/chapter', 'ChapterController@store');
        Route::post('/classroom/{id}/addstudent', 'ClassroomController@sendInvite');
        Route::post('/create/test', 'TestController@store');
        Route::post('/test/{id}/edit/addquestion', 'TestController@addQuestion');
        Route::post('/test/{testid}/edit/question/{questionid}', 'TestController@editQuestion');
        Route::post('/acceptclassrequest/{fromuserid}/{classid}', 'ClassroomController@acceptRequest');
        Route::post('/declineclassrequest/{fromuserid}/{classid}', 'ClassroomController@declineRequest');
        Route::post('/create/chapterset', 'ChapterSetController@store');

        //MODIFIERS
        Route::delete('/classroom/delete/{id}/{harddelete?}', 'ClassroomController@destroy');
        Route::patch('/classroom/restore/{id}', 'ClassroomController@restore');
    });

//MODERATOR ONLY ROUTES
    Route::group(['middleware' => 'moderator'], function () {
        Route::get('/modpanel', 'ModeratorController@index');
    });

//ADMINISTRATOR ONLY ROUTES
    Route::group(['middleware' => 'administrator'], function () {
        Route::get('/adminpanel', 'AdminController@index');
    });
});

/* NON-AUTH ROUTES */
Route::group(['before' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::post('/auth/login', 'HomeController@login');
    Route::post('/auth/register', 'HomeController@register');
    Route::get('/auth/register', 'Auth\AuthController@getRegister');
    Route::get('/auth/logout', 'Auth\AuthController@getLogout');
    Route::get('/auth/login', 'Auth\AuthController@getLogin');
    Route::post('/password/email', 'Auth\PasswordController@postEmail');
    Route::get('/learn', 'LearnController@base');
    Route::get('/learn/{subject}', 'SubjectController@index');
    Route::get('/learn/{subject}/{course}', 'CourseController@index');
    Route::get('/learn/{subject}/{course}/{chapter}/{lesson}', 'LessonController@index');
    Route::get('/user/{id}', 'ProfileController@show');
    Route::get('/forums', 'ForumController@index');
    Route::get('/chapter/{id}', 'ChapterController@show');
    Route::get('/lesson/{id}', 'LessonController@show');
    Route::get('/forum/{id}', 'ForumController@show');
    Route::post('/createthread/{id}', 'ThreadController@store');
    Route::get('/forums/forum/thread/{id}', 'ThreadController@show');
    Route::post('/forum/thread/{id}/addcomment', 'PostController@store');


    //IMAGE SERVES
    Route::get('/images/user/{id}', 'ProfileController@imageServe');
    Route::get('/images/user/{id}/header', 'ProfileController@headerImageServe');
    Route::get('/images/classroom/{id}/header', 'ClassroomController@headerImageServe');
});
