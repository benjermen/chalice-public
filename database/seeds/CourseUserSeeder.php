<?php

use Illuminate\Database\Seeder;
use App\Course as Course;
use App\User;

class CourseUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->courseUsers();
    }

    public function courseUsers()
    {
		$courseids = Course::lists('id')->toArray();
		$userids = User::where('privilege', 0)->lists('id')->toArray();

		foreach($userids as $user)
		{
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
			DB::statement('insert into course_user values (?,?)', [Faker\Factory::create()->randomElement($courseids), $user]);
		}
    }
}

