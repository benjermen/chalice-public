<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 'lessons';

    public function comments()
    {
        return $this->hasMany('App\Comment', 'lesson_id', 'id')->orderBy('updated_at', 'desc');
    }
}
