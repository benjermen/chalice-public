@extends('layouts.hub')

@section('body')
    @if($myprofile)
        <div class="row header-tile" style="background:url('/images/user/{{ $user->id }}/header') no-repeat 0 0;">
            <div class="m-a-0 row">
                <div class="col-md-6" style="margin-left: 25%;">
                    <img src="/images/user/{{ Auth::user()->id }}" alt="" class="profile-img">
                </div>
                <div class="col-md-2 img-upload">
                    <form action="/user/updateheader/{{ $user->id }}" id="header-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <span class="btn btn-outline-secondary btn-file btn-sm">
                        Header Image <input type="file" name="image" id="header-image-input">
                    </span>
                    </form>
                </div>
            </div>
            <div class="m-a-0 row wrapper-name">
                <h2 class="fullname">{{ Auth::user()->fullName() }}</h2>
            </div>
        </div>
    @endif
    @if(!$myprofile)
        <div class="row header-tile" style="background:url('/images/user/{{ $user->id }}/header') no-repeat 0 0;">
            <div class="m-a-0 row">
                <div class="col-md-6" style="margin-left: 25%;">
                    <img src="/images/user/{{ $user->id }}" alt="" class="profile-img">
                </div>
            </div>
            <div class="m-a-0 row wrapper-name">
                <h2 class="fullname">{{ $user->fullName() }}</h2>
            </div>
        </div>
        <form action="/sendfriendrequest/{{ $user->id }}" method="post">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-primary" value="Send Friend Request">
        </form>
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('successmsg') }}
            </div>
        @endif
        @if(strlen($errors->friendrequest->first()) > 0)
            <div class="alert alert-danger">
                {{ $errors->friendrequest->first() }}
            </div>
        @endif
    @endif
@stop