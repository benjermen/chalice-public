@extends('layouts.app')

@section('body')
    <div class="row text-center m-b-2">
        <h3>{{ $test->name }}</h3>
    </div>

    <div class="col-md-6 m-b-3">
        <a href="/test/{{ $test->id }}/edit/addquestion">
            <div class="list-group-item text-center">
                <i class="plus list-group-item-heading fa fa-plus grey"></i>

                <p class="list-group-item-text">Add Question.</p>
            </div>
        </a>
    </div>

    <div class="col-md-6">
        @include('partials.question-list')
    </div>

@stop