<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User as User;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function send($fromuser = null, $touser, $purpose, $append = null, $optid = null)
    {
        $notification = new self;
        $notification->user_id = $touser;
        $notification->from_user_id = $fromuser;
        $notification->body = self::getBody($purpose) . $append;
        $notification->opt_id = $optid;
        $notification->purpose = $purpose;
        $notification->extension = self::getExtension($purpose);
        $notification->save();
    }

    public static function sendModIssue($fromuser)
    {
        $modIds = [];
        $mods = User::where('privilege', '=', 2)->lists('id');

        foreach($mods as $mod)
        {
            array_push($modIds, $mod);
        }

        $modId = $modIds[array_rand($modIds, 1)];

        self::send($fromuser, $modId, 2);
    }

    public static function getBody($purpose)
    {
        $fullname = Auth::user()->fullName();

        switch($purpose)
        {
            case 1:
                return $fullname . ' has sent you a friend request!';
            break;

            case 2:
                return $fullname . ' is a pending teacher!';

            case 10:
                return 'You have received a message!';
            break;

            case 11:
                return 'You have been invited to ';
            break;

            case 12:
                return $fullname . ' has requested access to ';

            default:
                return 'You have a notification.';
        }
    }

    public static function getExtension($purpose)
    {
        switch($purpose)
        {
            case 1:
                return '/notifications';
            break;

            case 2:
                return '/modpanel';

            case 10:
                return '/messages';
            break;

            case 11:
                return '/messages';
            break;

            default:
                return '/notifications';
        }
    }
}
