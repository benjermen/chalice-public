<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Message as Message;
use App\Conversation as Conversation;
use App\User;
use Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($conversationid, $body, $fromuser, $touser)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        if(!isset($conversationid))
        {
            $convExists = false;
            $user = User::find(Auth::user()->id);
            $toUser = User::find($request->route('id'));
            $fromUserConversations = $user->conversations()->lists('id');
            $toUserConversations = $toUser->conversations()->lists('id');

            foreach($fromUserConversations as $convid)
            {
                for($i = 0; $i < count($toUserConversations); $i++)
                {
                    if($toUserConversations[$i] == $convid)
                    {
                        $conversation = Conversation::find($convid);
                        $convExists = true;
                    }
                }
            }

            if($convExists)
            {
                $message = new Message;
                $message->conversation_id = $conversation->id;
                $message->body = $request->input('body');
                $message->from_user_id =$user->id;
                $message->to_user_id = $toUser->id;
                $message->save();

                $conversation->message_count++;
                $conversation->save();
            }
            else
            {
                $conversation = new Conversation;
                $message = new Message;

                $conversation->message_count = 1;
                $conversation->save();

                $message->conversation_id = $conversation->id;
                $message->body = $request->input('body');
                $message->from_user_id =$user->id;
                $message->to_user_id = $toUser->id;

                $message->save();

                $user->conversations()->attach($conversation->id);
                $toUser->conversations()->attach($conversation->id);

                Notification::send($user->id, $toUser->id, 3);
            }
        }
        else
        {

        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
