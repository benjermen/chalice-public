<!DOCTYPE html>
<html lang="en" class="full-height">
    <head>
        @section('meta')
            @include('partials.meta')
        @show
    </head>

    <body class="app full-height">
        <div class="container-fluid full-height">
            <div class="row topbar-nav">

                    @section('topbar')
                        @include('modules.topbar')
                    @show

            </div>
            <div class="row full-height">
                <div class="p-l-0 full-height fixed-sidebar">
                    @section('nav')
                        @include('modules.nav')
                    @show
                </div>
                <div class="fixed-content content-padding">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="/createthread"><input type="submit" class="btn btn-primary btn-block" value="Create a Thread"></a>
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" class="btn btn-primary btn-block" value="My Forums">
                    @yield('body')
                </div>
            </div>
        </div>

        @section('footer')
            @include('partials.footer')
        @show
    </body>
</html>
