<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(ChapterSeeder::class);
        $this->call(LessonSeeder::class);
        $this->call(ClassroomSubjectSeeder::class);
        $this->call(ForumSeeder::class);
        $this->call(CourseUserSeeder::class);

        Model::reguard();
    }
}
