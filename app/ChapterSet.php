<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChapterSet extends Model
{
    use SoftDeletes;

    public function creator()
    {
        return $this->belongsTo('App\User');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter', 'set_id', 'id');
    }
}
