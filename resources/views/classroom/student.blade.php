@extends('layouts.hub')

@section('body')
    <div class="row header-tile" style="background:url('/images/classroom/{{ $classroom->id }}/header') no-repeat 0 0;">
        <div class="m-a-0 row wrapper-classname">
            <div class="col-lg-10 col-md-8">
                <h2>{{ $classroom->name }}</h2>
            </div>
        </div>
        <div class="m-a-0 row wrapper-classdescrip">
            <h4>{{ $classroom->description }}</h4>
        </div>
    </div>

    <div class="hub-column col-md-6">
        <div class="row heading-center">
            <h2>Chapters</h2>
        </div>
        <div class="list-group">

            <br>
            @if(count($classroom->chapters) > 0)
                @foreach($classroom->chapters as $chapter)
                    <a href="/chapter/{{ $chapter->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $chapter->name }}</h4>
                            <p class="list-group-item-text">{{ $chapter->description }}</p>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Chapters.</h4>

                    <p class="list-group-item-text">No chapters in this classroom.</p>
                </div>
            @endif
        </div>
    </div>

    <div class="hub-column col-md-6">
        <div class="row heading-center">
            <h2>Assignments</h2>
        </div>
        <div class="list-group">

            <br>

            @if(count($classroom->assignments) > 0)

                @foreach($classroom->assignments as $assignment)
                    <a href="/assignment/{{ $assignment->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $assignment->name }}</h4>
                            <p class="list-group-item-text">{{ $assignment->description }}</p>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Assignments.</h4>

                    <p class="list-group-item-text">No Assignments in this classroom.</p>
                </div>
            @endif
        </div>
    </div>
@stop