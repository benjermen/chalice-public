@extends('layouts.app')

@section('body')
    <div class="m-a-0 row wrapper-classname">
        <h2>{{ $assignment->name }}</h2>
    </div>
    <div class="m-a-0 row wrapper-classdescrip">
        <h4>{{ $assignment->description }}</h4>
    </div>

    <br>

    @if(!is_null($assignment->test_id))
        <div class="col-md-12">
            <strong>Attempts: {{ $attempts }}</strong>
        </div>
        <div class="col-md-12">
            <a href="/attempt/{{ $assignment->test_id }}/assignment/{{ $assignmentid }}" class="btn btn-primary">Attempt Test</a>
        </div>

    @elseif(!is_null($assignment->file_name))
        <a href="/assignment/{{ $assignment->id }}/file" class="btn btn-primary">Download Assignment File</a>
    @endif
@stop