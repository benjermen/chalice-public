@extends('layouts.app')

@section('body')
    <div class="title">
        <h1>{{ $course->name }}</h1>
    </div>
    <div class="summary-box">
        <h2>{{ $course->description }}</h2>
    </div>
    <div class="chapter-box">
        <div id="accordion" role="tablist" aria-multiselectable="true">
            @foreach($course->chapters as $chapter)
                <?php if(!isset($i)) {$i = 1;} ?>
                    <div class="lesson-title">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{ $i }}">
                                <h1 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $i }}" aria-expanded="false"
                                        aria-controls="collapse{{ $i }}">
                                        {{ $chapter->name }}
                                    </a>
                                </h1>
                            </div>
                        </div>
                     </div>

                    <div id="collapse{{ $i }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{ $i }}">
                            <h4>
                                @foreach($chapter->lessons as $lesson)
                                    <li class="list-unstyled"><a href="/learn/{{ $subject_name }}/{{ $course->name }}/{{ $chapter->name }}/{{ $lesson->name }}">{{ $lesson->name }}</a></li>
                                @endforeach
                            </h4>
                    </div>
                <?php $i++; ?>
            @endforeach
        </div>
    </div>
@stop

