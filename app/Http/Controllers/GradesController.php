<?php

namespace App\Http\Controllers;

use App\Classroom;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class GradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->teacher())
        {
            $classes = Auth::user()->classes()->get();
            $classrooms = Auth::user()->classrooms()->get();
            $classroom = Classroom::with('students', 'assignments.grades')->first();
            $class = Auth::user()->classes()->first();
            if(Auth::user()->id == $classroom->teacher_id)
            {

                return view('gradebook.classroom')
                    ->with('classrooms', $classrooms)
                    ->with('classes', $classes)
                    ->with('class', $class)
                    ->with('classroom', $classroom);
            }
            else
            {
                $classes = Auth::user()->classes()->get();
                $classrooms = Auth::user()->classrooms()->get();
                $class = Auth::user()->classes()->first();

                return view('gradebook')
                    ->with('classrooms', $classrooms)
                    ->with('classes', $classes)
                    ->with('class', $class);
            }
        }
        else
        {
            $classes = Auth::user()->classes()->get();
            $class = Auth::user()->classes()->first();

            return view('gradebook')
                ->with('classes', $classes)
                ->with('class', $class);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function showClassroomGrades($userid, $classroomid)
    {
        if(Auth::user()->id == $userid)
        {
            if(Auth::user()->teacher())
            {
                $classes = Auth::user()->classes()->get();
                $classrooms = Auth::user()->classrooms()->get();
                $classroom = Classroom::with('students', 'assignments.grades')->find($classroomid);
                $class = Auth::user()->classes()->find($classroomid);
                if($userid == $classroom->teacher_id)
                {
                    return view('gradebook.classroom')
                        ->with('classrooms', $classrooms)
                        ->with('classes', $classes)
                        ->with('class', $class)
                        ->with('classroom', $classroom);
                }
                else
                {
                    $classes = Auth::user()->classes()->get();
                    $classrooms = Auth::user()->classrooms()->get();
                    $class = Auth::user()->classes()->find($classroomid);

                    return view('gradebook')
                        ->with('classrooms', $classrooms)
                        ->with('classes', $classes)
                        ->with('class', $class);
                }
            }
            else
            {
                $classes = Auth::user()->classes()->get();
                $class = Auth::user()->classes()->find($classroomid);

                return view('gradebook')
                    ->with('classes', $classes)
                    ->with('class', $class);
            }
        }

        return redirect('/hub');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grades = Auth::user()->grades()->where('classroom_id', '=', $id)->get();
        $classes = Auth::user()->classes()->get();
        $classrooms = Auth::user()->classrooms()->get();

        return view('gradebook')
            ->with('classes', $classes)
            ->with('classrooms', $classrooms)
            ->with('grades', $grades);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
