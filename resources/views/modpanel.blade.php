@extends('layouts.app')

@section('body')
    <div class="row list-box">
        <div class="col-md-6 m-a-1 issue-box">
            <ul class="list-unstyled">
                @foreach($issues as $issue)
                    <li class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                {{ $issue->body }}
                            </div>
                            <div class="col-md-4">
                                <a href="/issue/{{ $issue->id }}">
                                    <span class="fa fa-user-secret"></span>
                                        <span>
                                            <strong>Investigate</strong>
                                        </span>
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop

