<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\User as User;
use Auth;
use DB;
use Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id > 0)
        {
            $user = User::find($id);

            if (Auth::check()) {
                if ($id == Auth::user()->id) {
                    $myprofile = true;
                } else {
                    $myprofile = false;
                }
            } elseif (!Auth::check()) {
                return view('profile')
                    ->with('user', $user)
                    ->with('myprofile', false);
            }

            return view('profile')
                ->with('user', $user)
                ->with('myprofile', $myprofile);
        }
        else
        {
            return redirect('hub');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'oldPassword' => 'required|min:6|different:newPassword',
            'newPassword' => 'required|same:passwordConfirmation|min:6|max:255',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
            'oldPassword:different' => 'Your new password cannot be identical to your old password.',
            'newPassword:same' => 'Your passwords do not match.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            return redirect('/settings')->withErrors($validator->messages(), 'settings');
        }

        if(Auth::attempt(['email' => Auth::user()->email, 'password' => $request->input('oldPassword')]))
        {
            if($request->input('newPassword') == $request->input('passwordConfirmation'))
            {
                $user->password = bcrypt($request->input('newPassword'));
                $user->save();
                return redirect('/settings')->with('passwordSuccess', 'Password changed successfully.');
            }
        }
        else
        {
            return redirect('/settings')->with('passwordError', 'Incorrect Current Password.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEmail(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'newEmail' => 'required|email|unique:users,email',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
            'newEmail:email' => 'This field must contain a email.',
            'newEmail:unique' => 'There is already a Chalice account with this email.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            return redirect('/settings')->withErrors($validator->messages(), 'settings');

        }

        $user->email = $request->input('newEmail');
        $user->save();

        return redirect('/settings')->with('emailSuccess', 'Email changed successfully.');

    }

    public function image(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'image' => 'required|image|mimes:jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/settings')->withErrors($validator->messages(), 'settings');
        }

        $file = $request->file('image');
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $filename = '';
        $charLen = strlen($chars) - 1;

        for ($i = 0; $i < 15; $i++)
        {
            $filename .= $chars[rand(0, $charLen)];
        }

        $filename = $filename . $file->getClientOriginalName();

        if(!$user->profile_image == "")
        {
            \Storage::delete('img/users/' . $user->id . '/' . $user->profile_image);
        }

        $user->profile_image = $filename;
        $user->save();
        \Storage::put('img/users/' . $user->id . '/' . $filename, File::get($file));

        return redirect('/hub')->with('imgSuccess', 'Profile Image Changed.');
    }

    public function updateHeaderImage(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'image' => 'required|image|mimes:jpeg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages(), 'headerimg');
        }

        $file = $request->file('image');
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $filename = '';
        $charLen = strlen($chars) - 1;

        for ($i = 0; $i < 15; $i++)
        {
            $filename .= $chars[rand(0, $charLen)];
        }

        $filename = $filename . $file->getClientOriginalName();

        if(!$user->header_image == "")
        {
            \Storage::delete('img/users/' . $user->id . '/header/' . $user->header_image);
        }

        $user->header_image = $filename;
        $user->save();
        \Storage::put('img/users/' . $user->id . '/header/' . $filename, File::get($file));

        return redirect('/hub')->with('imgSuccess', 'Header Image Changed.');
    }

    public function imageServe($id)
    {
        $filepath = storage_path('app') . User::find($id)->getProfileImage();
        return \Response::download($filepath);
    }

    public function headerImageServe($id)
    {
        $filepath = storage_path('app') . User::find($id)->getHeaderImage();
        return \Response::download($filepath);
    }

    public function query()
    {
        $users = DB::table('users')->select('id', 'first_name', 'last_name', 'invite_id')->get();

        foreach($users as $user)
        {
            $usernames[] = ['id' => $user->id, 'name' => $user->first_name . ' ' . $user->last_name, 'inviteid' => $user->invite_id];
        }

        return response()->json($usernames);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
