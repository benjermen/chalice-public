<?php

use Illuminate\Database\Seeder;
use App\Subject as Subject;
use Carbon\Carbon;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->subjects();
    }

    public function subjects()
    {

    	//Initialize
    	$faker = Faker\Factory::create();

    	$subjects = [
    		'Math' => '/img/subjects/math.jpg',
    		'Science' => '/img/subjects/science.jpg',
    		'History' => '/img/subjects/history.jpg',
    		'Economics' => '/img/subjects/economics.jpg',
    		'Computing' => '/img/subjects/computing.jpg',
    		'Test Prep' => '/img/subjects/testprep.jpg',
    	];

    	foreach($subjects as $subjectName => $img)
    	{
    		$subject1 = new Subject;
    		$subject1->name = $subjectName;
    		$subject1->description = $faker->text;
            $subject1->img = $img;
    		$subject1->created_at = Carbon::now('America/Detroit');
        	$subject1->updated_at = Carbon::now('America/Detroit');
    		$subject1->save();
    	}
    }
}
