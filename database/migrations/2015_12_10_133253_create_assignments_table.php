<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Classification specifies Multiple choice, test, pdf, etc.
        Schema::create('assignments', function(Blueprint $table){
            $table->increments('id');
            $table->integer('classroom_id');
            $table->string('name');
            $table->string('description');
            $table->integer('test_id')->nullable();
            $table->string('file_name')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assignments');
    }
}
