@extends('layouts.app')

@section('body')
    <form action="/thread/{{ $thread->id }}/delete" class="left" method="post">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}

        <input type="submit" class="btn btn-primary" value="Delete">
    </form>

    <div class="container-fluid">
        <h1>{{$thread->title}}</h1>
        <hr>
        <h5>Posted By <a href="/user/{{$user->id}}">{{$user->fullName()}}</a> <small>At {{$thread->created_at}}</small></h5>

        <h2>{{$thread->body}}</h2>
        <br>
        @foreach($thread->posts as $post)
            <ul class="list-unstyled forum-page list-group">
                    <li class="list-group-item" style="margin-bottom: 3px;">
                        <h4><a href="/user/{{$user->id}}"> {{$user->fullName()}}</a></h4>
                        <h6><small>At {{$post->created_at}}</small></h6>
                            <h5 class="body-wrap">{{ $post->body }}</h5>
                </li>
            </ul>
            <div>
            </div>
        @endforeach
        <br>
        <form action="/forums/thread/{{ $thread->id}}/addcomment" method="post">
            {{ csrf_field() }}
            <div class="form-group col-md-4">
               <textarea name="body" rows="5" cols="50">{{ Request::old('body') }}</textarea>
                <input type="submit" class="btn btn-primary" value="Submit a Comment">
                @if(strlen($errors->post->first()) > 0)
                    <div class="alert alert-danger fade in">
                        {{ $errors->post->first() }}
                    </div>
                @endif
            </div>
        </form>
    </div>
@stop