var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('notifAction', function(err, count) {

});

redis.on('message', function(channel, message) {
    var messagejson = JSON.parse(message);

    console.log('Message Recieved On Channel: ' + channel + ' : Contents : ' +  message);


    io.emit(channel + ':' + messagejson.event + ':' + messagejson.data.channel, messagejson.data.notification);
});

http.listen(3000, function(){
    console.log('Listening on Port 3000');
});