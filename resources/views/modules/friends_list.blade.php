@foreach(Auth::user()->getOnlineFriends() as $friend)
    <div class="row content-row">
        <div class="col-md-2">
            <a href="/user/{{ $friend->id }}">
                <img src="/images/user/{{ $friend->id }}" class="img-circle" alt="">
            </a>
        </div>
        <div class="col-md-8">
            <h3>{{ $friend->fullName() }}</h3>
        </div>
        <div class="col-md-2">
            <i class="fa fa-ellipsis-v"></i>
        </div>
    </div>
@endforeach