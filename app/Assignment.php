<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function grades()
    {
        return $this->hasMany('App\Grade', 'assignment_id', 'id');
    }

    public function test()
    {
        return $this->belongsToMany('App\Test', 'assignment_test', 'assignment_id', 'test_id');
    }
}
