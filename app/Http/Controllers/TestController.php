<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Test;
use Carbon\Carbon;
use Auth;
use App\Question;
use App\Attempt;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create.test');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:80',
            'description' => 'required|max:300',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/create/test')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'testErrors');
        }

        $test = new Test;
        $test->creator_id = Auth::user()->id;
        $test->name = $request->input('name');
        $test->description = $request->input('description');
        $test->created_at = Carbon::now('America/Detroit');
        $test->save();

        return redirect('/test/' . $test->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('test.edit')
            ->with('test', Test::with('questions')->find($id));
    }

    public function questionAdd($id)
    {
        return view('test.addquestion')
            ->with('test', Test::with('questions')->find($id));
    }

    public function questionEdit($testid, $questionid)
    {
        $test = Test::with('questions')->find($testid);
        $question = Question::find($questionid);

        return view('test.editquestion')
            ->with('test', $test)
            ->with('question', $question);
    }

    public function addQuestion(Request $request)
    {
        $question = new Question;
        $question->t_f = 0;

        if($request->input('t-f') != 'on')
        {
            $rules = [
                'question' => 'required|max:300',
                'ans-a' => 'required|max:150',
                'ans-b' => 'required|max:150',
                'ans-c' => 'required|max:150',
                'ans-d' => 'required|max:150',
                'correct' => 'required',
            ];
        }
        else
        {
            if($request->input('ans-a') != "" || $request->input('ans-b') != "" || $request->input('ans-c') != "" || $request->input('ans-d') != "")
            {
                return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                    ->withInput($request->input('question'))
                    ->with('error', 'You cannot have any answer fields if you are making a true/false question.');
            }

            $rules = [
                'question' => 'required|max:300',
                'correct' => 'required',
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'questionErrors');
        }

        if($request->input('t-f') == 'on')
        {
            if($request->input('correct') == 'c' || $request->input('correct') == 'd')
            {
                return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                    ->withInput($request->all())
                    ->with('error', 'You must choose true or false as the answer on a true/false question.');
            }

            $question->t_f = 1;
        }

        $test = Test::with('questions')->find($request->route('id'));

        $question->test_id = $request->route('id');
        $question->set_id = count($test->questions) + 1;
        $question->question = $request->input('question');
        $question->answer = $request->input('correct');
        if($request->input('t-f') != 'on')
        {
            $question->ans_a = $request->input('ans-a');
            $question->ans_b = $request->input('ans-b');
            $question->ans_c = $request->input('ans-c');
            $question->ans_d = $request->input('ans-d');
        }
        else
        {
            $question->ans_a = null;
            $question->ans_b = null;
            $question->ans_c = null;
            $question->ans_d = null;
        }
        $question->created_at = Carbon::now('America/Detroit');

        $question->save();

        return redirect('/test/' . $request->route('id') . '/edit/addquestion')
            ->with('success', 'Question Created Successfully.');
    }

    public function editQuestion(Request $request)
    {
        $question = Question::find($request->route('questionid'));

        $question->t_f = 0;

        if($request->input('t-f') != 'on')
        {
            $rules = [
                'question' => 'required|max:300',
                'ans-a' => 'required|max:150',
                'ans-b' => 'required|max:150',
                'ans-c' => 'required|max:150',
                'ans-d' => 'required|max:150',
                'correct' => 'required',
            ];
        }
        else
        {
            if($request->input('ans-a') != "" || $request->input('ans-b') != "" || $request->input('ans-c') != "" || $request->input('ans-d') != "")
            {
                return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                    ->withInput($request->input('question'))
                    ->with('error', 'You cannot have any answer fields if you are making a true/false question.');
            }

            $rules = [
                'question' => 'required|max:300',
                'correct' => 'required',
            ];
        }

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'questionErrors');
        }

        if($request->input('t-f') == 'on')
        {
            if($request->input('correct') == 'c' || $request->input('correct') == 'd')
            {
                return redirect('/test/' . $request->route('id') . '/edit/addquestion')
                    ->withInput($request->all())
                    ->with('error', 'You must choose true or false as the answer on a true/false question.');
            }

            $question->t_f = 1;
        }

        $test = Test::with('questions')->find($request->route('testid'));

        $question->test_id = $request->route('testid');
        $question->question = $request->input('question');
        $question->answer = $request->input('correct');
        if($request->input('t-f') != 'on')
        {
            $question->ans_a = $request->input('ans-a');
            $question->ans_b = $request->input('ans-b');
            $question->ans_c = $request->input('ans-c');
            $question->ans_d = $request->input('ans-d');
        }
        else
        {
            $question->ans_a = null;
            $question->ans_b = null;
            $question->ans_c = null;
            $question->ans_d = null;
        }
        $question->updated_at = Carbon::now('America/Detroit');

        $question->save();

        return redirect('/test/' . $request->route('testid') . '/edit/question/' . $request->route('questionid'))
            ->with('success', 'Question Edited Successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
