@extends('layouts.home')

@section('body')

    <div class="container m-t-3">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <h1 class="display-3 text-center text-uppercase m-b-3">Chalice</h1>

                @include('modules/register')
            </div>
        </div>
    </div>

@stop