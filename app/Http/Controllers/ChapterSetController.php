<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ChapterSet;
use Auth;
use Carbon\Carbon;

class ChapterSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create.chaptersetcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'max:80',
            'description' => 'max:300',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect()->back()->with('error', $validator->messages()->first());
        }

        $chapterset = new ChapterSet;
        $chapterset->name = $request->input('name');
        $chapterset->description = $request->input('description');
        $chapterset->creator_id = Auth::user()->id;
        $chapterset->created_at = Carbon::now('America/Detroit');
        $chapterset->save();

        return redirect('/manage')->with('success', 'Chapter Set Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('chapterset')
            ->with('chapterset', ChapterSet::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
