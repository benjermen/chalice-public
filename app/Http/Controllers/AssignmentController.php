<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Auth;
use App\Assignment;
use App\Test;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classrooms = Auth::user()->classrooms()->get();
        $tests = Auth::user()->tests()->get();

        return view('create.assignment')
            ->with('tests', $tests)
            ->with('classrooms', $classrooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assignment = new Assignment;
        $fileExists = false;

        if($request->has('test') && $request->hasFile('file'))
        {
            return redirect('/create/assignment')
                ->withInput($request->all())
                ->with('testError', 'You cannot have a test and a file in one assignment.');
        }
        elseif($request->has('test'))
        {
            $assignment->test_id = $request->input('test');

            $rules = [
                'classroom' => 'exists:classrooms,id',
                'name' => 'required|max:30',
                'description' => 'required|max:80',
                'test' => 'exists:tests,id',
            ];
        }
        elseif($request->hasFile('file'))
        {
            $rules = [
                'classroom' => 'exists:classrooms,id',
                'name' => 'required|max:30',
                'description' => 'required|max:80',
                'file' => 'max:8000',
            ];

            $fileExists = true;
        }
        else
        {
            return redirect('/create/assignment')
                ->withInput($request->all())
                ->with('testError', 'You need content in your assignment!');
        }

        $validator = \Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/create/assignment')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'assignment');
        }

        $assignment->classroom_id = $request->input('classroom');
        $assignment->name = $request->input('name');
        $assignment->description = $request->input('description');

        if($fileExists)
        {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $assignment->file_name = $filename;
        }

        $assignment->save();

        if($fileExists)
        {
            \Storage::put('assignments/' . $assignment->id . '/' . $filename, File::get($file));
        }
        else
        {
            $assignment->test()->attach($request->input('test'));
        }

        return redirect('/classroom/' . $request->input('classroom'))
            ->with('success', 'Assignment Created Successfully.');
    }

    public function downloadFile($id)
    {
        $assignment = Assignment::find($id);

        if($assignment->file_name == '')
        {
            return redirect()->back();
        }
        else
        {
            $filepath = storage_path('app') . '/assignments/' . $assignment->id . '/' . $assignment->file_name;
            return \Response::download($filepath);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assignment = Assignment::find($id);
        $classroom = \App\Classroom::with('students')->find($assignment->classroom_id);

        if(Auth::user()->id == $classroom->teacher_id)
        {
            if(!is_null($assignment->test_id))
            {
                $attempts = Auth::user()->attemptCount($assignment->test_id);

                return view('assignment')
                    ->with('assignmentid', $id)
                    ->with('attempts', $attempts)
                    ->with('assignment', $assignment);
            }

            return view('assignment')
                ->with('assignment', $assignment);
        }

        foreach($classroom->students as $student)
        {
            if(Auth::user()->id == $student->id)
            {
                if(!is_null($assignment->test_id))
                {
                    $attempts = Auth::user()->attemptCount($assignment->test_id);

                    return view('assignment')
                        ->with('assignmentid', $id)
                        ->with('attempts', $attempts)
                        ->with('assignment', $assignment);
                }
                return view('assignment')
                    ->with('assignment', $assignment);
            }
        }

        return redirect('/hub');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacherid = Assignment::find($id)->classroom()->value('teacher_id');

        if(Auth::user()->id == $teacherid)
        {
            $assignment = Assignment::find($id);

            if($assignment->file_name != '')
            {
                \Storage::deleteDirectory('assignments/' . $assignment->id);
            }

            $assignment->delete();

            return redirect()->back();
        }

        return redirect()->back();
    }
}
