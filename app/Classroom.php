<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;

    protected $table = 'classrooms';

    public function chapters()
    {
        return $this->hasMany('App\Chapter', 'classroom_id', 'id');
    }

    public function students()
    {
        return $this->belongsToMany('App\User')->withPivot('user_id', 'classroom_id');
    }

    public function teacher()
    {
    	return $this->belongsTo('App\User', 'id', 'teacher_id');
    }

    public function assignments()
    {
        return $this->hasMany('App\Assignment', 'classroom_id', 'id');
    }

    public function getHeaderImage()
    {
        if($this->header_image != "" && \Storage::disk(ENV('STORAGE_DISK'))->has('/img/classrooms/' . $this->id . '/header/' . $this->header_image))
        {
            return '/img/classrooms/' . $this->id . '/header/' . $this->header_image;
        }

        return '/img/classroomheaderdefault.jpg';
    }


    public static function generateJoinCode() {
        $randString = '';
        $length = 8;
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charsLength = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $randString .= $chars[rand(0, $charsLength - 1)];
        }
        return $randString;
    }
}
