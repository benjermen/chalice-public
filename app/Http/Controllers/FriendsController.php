<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notification as Notification;
use App\User as User;
use Auth;

class FriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('friends')
                ->with('friends', Auth::user()->friends()->get());
    }

    public function sendRequest($touserid)
    {
        $user = Auth::user();
        $touser = User::find($touserid);
        $friendrequestpurpose = 1;

        if($touser->notifications()
            ->where('user_id', '=', $touserid)
            ->where('from_user_id', '=', $user->id)
            ->where('purpose', '=', $friendrequestpurpose)
            ->exists() || $user->notifications()
                ->where('user_id', '=', $user->id)
                ->where('from_user_id', '=', $touserid)
                ->where('purpose', '=', $friendrequestpurpose)
                ->exists())
        {
            $friendRequestExists = true;
        }
        else{
            $friendRequestExists = false;
        }

        if($user->friends->contains($touserid))
        {
            return redirect()->back()->withErrors('You are already friends with that person.', 'friendrequest');
        }
        elseif($friendRequestExists)
        {
            return redirect()->back()->withErrors('There is already a friend request pending.', 'friendrequest');
        }
        elseif(!$user->friends->contains($touserid))
        {
            Notification::send($user->id, $touserid, $friendrequestpurpose);
            return redirect()->back()->with('successmsg', 'Friend Request Sent.')
                                        ->with('success', true);
        }
    }

    public function acceptRequest($fromuser)
    {
        $friendrequestpurpose = 1;

        $friendRequest = Notification::where('from_user_id', '=', $fromuser)->where('user_id', '=', Auth::user()->id)
                                        ->where('purpose', '=', $friendrequestpurpose);

        $friendRequest1 = Notification::where('from_user_id', '=', Auth::user()->id)->where('user_id', '=', $fromuser)
            ->where('purpose', '=', $friendrequestpurpose);

        Auth::user()->friends()->attach($fromuser);
        User::find($fromuser)->friends()->attach(Auth::user()->id);

        $friendRequest->delete();
        $friendRequest1->delete();

        return redirect()->back();
    }

    public function declineRequest($fromuser)
    {
        $friendrequestpurpose = 1;

        $friendRequest = Notification::where('from_user_id', '=', $fromuser)->where('user_id', '=', Auth::user()->id)
            ->where('purpose', '=', $friendrequestpurpose);

        $friendRequest->delete();

        return redirect()->back();
    }

    public function destroy($friendid)
    {

        $friendpivot = Auth::user()->friends();
        $friendpivotinverse = User::find($friendid)->friends();

        $friendpivot->detach($friendid);

        $friendpivotinverse->detach(Auth::user()->id);

        return redirect()->back();
    }
}
