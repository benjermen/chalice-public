<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TeacherMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest())
        {
            return redirect()->guest('/')
                ->withErrors('You must be logged in to view that.', 'general');
        }
        elseif(Auth::user()->privilege != 1 && Auth::user()->privilege != 9 && Auth::user()->privilege != 5)
        {
            return redirect('hub');
        }

        return $next($request);
    }
}
