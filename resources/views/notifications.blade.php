@extends('layouts.app')

@section('body')
    @foreach($notifications as $notification)
        @if($notification->purpose == 1)
            <div class="row content-row">
                <div class="col-md-8 content-row-body">
                    {{ $notification->body }}
                </div>
                <div class="col-md-2">
                    <form action="/acceptfriendrequest/{{ $notification->from_user_id }}" method="post">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success" value="Accept">
                    </form>
                </div>
                <div class="col-md-2">
                    <form action="/declinefriendrequest/{{ $notification->from_user_id }}" method="post">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Decline">
                    </form>
                </div>
            </div>
        @endif
    @endforeach
@stop