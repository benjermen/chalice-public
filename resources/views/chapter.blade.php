@extends('layouts.app')

@section('body')
    <div class="container">

            <div class="list-group">
                <div class="list-group-item">
                    <h1 class="list-group-item-heading text-center p-t-1">{{ $chapter->name }}</h1>
                    <br>
                    <br>

                    <p class="list-group-item-text m-b-1">{{ $chapter->description }}</p>
                </div>
            </div>
        <br>
            <div class="row heading-center">
                <h2>Lessons</h2>
            </div>
            <div class="list-group">
                @if(count($chapter->lessons) == 0)
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading text-center m-b-1">No Lessons.</h4>
                            <p class="list-group-item-text">No lessons in this chapter yet!</p>
                        </div>
                    </div>
                @endif
                @foreach($chapter->lessons as $lesson)
                    <a href="/lesson/{{ $lesson->id }}" class="list-group-item">
                        <h4 class="list-group-item-heading text-center m-b-1">{{ $lesson->name }}</h4>

                        <p class="list-group-item-text">{{ $lesson->description }}</p>
                    </a>
                @endforeach
            </div>

    </div>
@stop