@extends('layouts.app')

@section('body')

    <h2>My Courses</h2>

    @foreach($mycourses as $course)
        <div class="col-lg-4 col-md-6 col-xs-12">
            <a href="/learn/{{ $course->subjects->name }}/{{ $course->name }}" class="pane btn-block">
                <div class="pane-image">
                    <img src="{{ $course->img }}" alt="">
                </div>
                <div class="pane-text">
                    <div class="pane-text-main">{{ $course->name }}</div>
                    <div class="pane-text-sub">View Chapters & Lessons</div>
                </div>
            </a>
        </div>
    @endforeach

@stop
