<div class="login">
    <ul class="nav nav-tabs nav-table text-center" role="tablist">
        <li href="#login" class="nav-item active cursor-pointer" aria-controls="login" role="tab" data-toggle="tab">Login</li>
        <li href="#sign-up" class="nav-item cursor-pointer" aria-controls="sign-up" role="tab" data-toggle="tab">Sign Up</li>
    </ul>

    <div class="tab-content m-t-2">
        <div role="tabpanel" class="tab-pane active" id="login">
            <form action="/auth/login" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="user-email" class="control-label m-b-0">Email</label>
                    <input class="form-control" maxlength="255" id="user-email" placeholder="example@example.com" type="email" size="50" name="email" value="{{ old('email') }}">
                    @if(strlen($errors->login->first('email')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->login->first('email') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="user-pw" class="control-label m-b-0">Password</label>
                    <input class="form-control" maxlength="255" id="user-pw" placeholder="Password" type="password" size="50" name="password">

                    @if(strlen($errors->login->first('password')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->login->first('password') }}
                        </div>
                    @endif
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" checked="checked"> Remember me
                    </label>
                </div>

                @if(count($errors->general) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->general->first() }}
                    </div>
                @endif

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" value="Login">
                </div>

                <div class="form-group">
                    <ul class="nav nav-table text-center" role="tablist">
                        <li href="#forgot" class="small nav-item forgot cursor-pointer" aria-controls="forgot" role="tab" data-toggle="tab">Forgot Password</li>
                    </ul>
                </div>

                <hr>

                
            </form>
        </div>

        <div role="tabpanel" class="tab-pane" id="sign-up">
            <form action="/auth/register" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="user-register-email" class="control-label m-b-0">Email</label>
                    <input class="form-control" type="email" id="user-register-email" placeholder="example@example.com" size="50" name="email" value="{{ old('email') }}">
                    @if(strlen($errors->signup->first('email')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->signup->first('email') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="user-register-first-name" class="control-label m-b-0">First Name</label>
                    <input class="form-control" type="text" id="user-register-first-name" placeholder="John" size="50" name="first_name" value="{{ old('first_name') }}">
                    @if(strlen($errors->signup->first('first_name')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->signup->first('first_name') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="user-register-last-name" class="control-label m-b-0">Last Name</label>
                    <input class="form-control" type="text" id="user-register-last-name" placeholder="Doe" size="50" name="last_name" value="{{ old('last_name') }}">
                    @if(strlen($errors->signup->first('last_name')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->signup->first('last_name') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="user-register-password" class="control-label m-b-0">Password</label>
                    <input class="form-control" maxlength="255" id="user-register-password" placeholder="Password" type="password" size="50" name="password">
                    @if(strlen($errors->signup->first('password')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->signup->first('password') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="user-register-password-confirmation" class="control-label m-b-0">Confirm password</label>
                    <input class="form-control" maxlength="255" id="user-register-password-confirmation" placeholder="Password" type="password" size="50" name="password_confirmation" />
                    @if(strlen($errors->signup->first('password_confirmation')) > 0)
                        <div class="alert alert-danger">
                            {{ $errors->signup->first('password_confirmation') }}
                        </div>
                    @endif
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="teacher"> Sign up as a Teacher
                    </label>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" value="Create!">
                </div>

               

                <div class="form-group">
                    <small>By creating an account you agree to our <a href="#" target="_blank" role="link">Terms & Conditions</a></small>
                </div>
            </form>
        </div>

        <div role="tabpanel" class="tab-pane" id="forgot">
            <form method="POST" action="/password/email">
                {!! csrf_field() !!}

                @if (count($errors->passreset) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->passreset->first() }}
                    </div>
                @endif

                <div>
                    Email
                    <input type="email" name="email" value="{{ old('email') }}">
                </div>

                <div>
                    <button type="submit">
                        Send Password Reset Link
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
