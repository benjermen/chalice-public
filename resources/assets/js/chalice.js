(function($) {

    $(".btn-slidein").click(function() {
        $(".fixed-sidebar").toggleClass("expanded");
        $(".app").toggleClass("expanded");
    });

})(jQuery);


$(document).ready(function(){

    //This is the slide function for the flash success and error messages

        $("#flash-alert").alert();
        $("#flash-alert").fadeTo(3000, 500).slideUp(500, function(){
            $("#flash-alert").alert('close');
    });



    //This is the typeahead suggestion for add a student to a class

    var users = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/queries/users/'
    });

    // initialize the bloodhound suggestion engine
    users.clearPrefetchCache();
    users.initialize();

    // instantiate the typeahead UI
    $('#tt-id .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 2
    },
    {
        source: users.ttAdapter(),
        name: 'users',
        displayKey: 'inviteid',
        templates: {
            empty: [
                '<div class="no-results tt-suggestion">No results.</div>'
            ].join('\n'),

            suggestion: function (data) {
                return '<div class="user-search-result"><img class="img-circle" src="/images/user/' + data.id + '"/><p class="suggestion-fullname">' + data.name + '</p></div>';
            }
        }
    });

    // instantiate the typeahead UI
    $('#tt-general-search .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        },
        {
            source: users.ttAdapter(),
            name: 'users',
            displayKey: 'name',
            templates: {
                empty: [
                    '<div class="no-results tt-suggestion">No results.</div>'
                ].join('\n'),

                suggestion: function (data) {
                    return '<div class="user-search-result"><a class="btn-block" href="/user/' + data.id + '"><img class="img-circle" src="/images/user/' + data.id + '"/><p class="suggestion-fullname">' + data.name + '</p></a></div>';
                }
            }
        });

    //This is the typeahead suggestion for requesting classroom access

    var classes = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/queries/classes/'
    });

    // initialize the bloodhound suggestion engine
    classes.clearPrefetchCache();
    classes.initialize();

    // instantiate the typeahead UI
    $('#tt-class-request .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        },
        {
            source: classes.ttAdapter(),
            name: 'classes',
            displayKey: 'joincode',
            templates: {
                empty: [
                    ''
                ].join('\n'),

                suggestion: function (data) {
                    return '<div class="class-search-result"><img class="img-circle" src="/images/user/' + data.teacher_id + '"/><p class="suggestion-fullname">' + data.name + '</p></div>';
                }
            }
        });
});