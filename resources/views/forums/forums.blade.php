@extends('layouts.app')

@section('body')
<div class="col-md-12">
    @foreach($forums as $forum)
    <ul class="list-unstyled forum-page list-group" >
        <li class="list-group-item" style="margin-bottom: 3px;">
            <a href="forum/{{ $forum->id }}">
                <span>{{ $forum->title }}</span>
            </a>
        </li>
    </ul>
        @endforeach
</div>
@stop