<?php

namespace App\Http\Controllers;

use App\Classroom as Classroom;
use App\User as User;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Notification;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create.classroom');
    }

    /**
     * Create a new Classroom instance in the database
     *
     * @param $data
     */
    public function store(Request $request)
    {
        $rules = [
            'class_name' => 'required|max:30',
            'class_description' => 'required|max:80',
        ];

        $messages = [
            'class_name.required' => 'The name field is required',
            'class_name.max' => 'The name field must be less than 30 characters.',
            'class_description.max' => 'The description field must be less than 80 characters.',
            'class_description.required' => 'The description field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails())
        {
            return redirect('/create/classroom')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'classroomcreate');
        }

        $classroom = new Classroom;

        $classroom->teacher_id = Auth::user()->id;
        $classroom->description = $request->input('class_description');
        $classroom->name = $request->input('class_name');
        $classroom->join_code = $this->generateRandAlpha(8);
        $classroom->save();

        $success = 'Classroom Created!';

        return redirect('/classroom/' . $classroom->id)
            ->with('classroom-success', $success);
    }


    public function generateRandAlpha($length) {
        $randString = '';
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charsLength = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $randString .= $chars[rand(0, $charsLength - 1)];
        }
        return $randString;
    }

    public function show($id)
    {
        $classroom = Classroom::with('students')->find($id);
        $user = Auth::user();

        //If the teacher is the teacher of the class
        if($classroom == null)
        {
            return redirect()->back()->with('error', 'That classroom is archived.');
        }

        if($user->id == $classroom->teacher_id)
        {
            return view('classroom.manage')
                ->with('classroom', $classroom);
        }
        else
        {
            foreach($classroom->students as $student)
            {
                if($student->id == $user->id)
                {
                    return view('classroom.student')
                        ->with('classroom', $classroom);
                }
            }
        }

        return redirect('/hub');
    }

    public function destroy($id, $harddelete = false)
    {
        $classroom = Classroom::withTrashed()->find($id);
        $success = false;
        $classroomname = $classroom->name;

        if(Auth::user()->id == $classroom->teacher_id)
        {
            $success = true;

            if($harddelete == 1)
            {
                $classroom->forceDelete();

                return redirect('/manage/archives')
                    ->with('hardDeleted', $success)
                    ->with('success', 'Classroom permanently deleted.')
                    ->with('classroomname', $classroomname);
            }

            $classroom->delete();

            return redirect('/manage')
                ->with('softDeleted', $success)
                ->with('success', 'Classroom moved to archives.')
                ->with('classroom', $classroom);
        }

        return redirect('/hub');
    }

    public function restore($id)
    {
        $classroom = Classroom::onlyTrashed()->find($id);
        $restored = 'The ' . $classroom->name . ' classroom has been restored!';

        if(Auth::user()->id == $classroom->teacher_id)
        {
            $classroom->restore();

            return view('manage')
                ->with('success', $restored);
        }
    }

    public function headerImageServe($id)
    {
        $filepath = storage_path('app') . Classroom::find($id)->getHeaderImage();
        return \Response::download($filepath);
    }

    public function updateHeaderImage(Request $request)
    {
        $classroom = Classroom::find($request->route('id'));

        $rules = [
            'image' => 'required|image|mimes:jpeg,jpg,png|max:3000',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect('/classroom/' . $request->route('id'))->withErrors($validator->messages(), 'headerimg');
        }

        $file = $request->file('image');
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $filename = '';
        $charLen = strlen($chars) - 1;

        for ($i = 0; $i < 15; $i++)
        {
            $filename .= $chars[rand(0, $charLen)];
        }

        $filename = $filename . $file->getClientOriginalName();

        if(!$classroom->header_image == "")
        {
            \Storage::delete('img/classrooms/' . $classroom->id . '/header/' . $classroom->header_image);
        }

        $classroom->header_image = $filename;
        $classroom->save();


        \Storage::put('img/classrooms/' . $classroom->id . '/header/' . $filename, \File::get($file));

        return redirect('/classroom/' . $request->route('id'))->with('imgSuccess', 'Header Image Changed.');
    }

    public function addStudent($classroomid)
    {
        $classroom = Classroom::find($classroomid);

        return view('classroom.addstudent')
            ->with('classroom', $classroom);
    }

    public function sendInvite(Request $request)
    {
        $touser = User::where('invite_id', '=', $request->input('name'))->first();
        $fromuser = Auth::user();
        $classroom = Classroom::find($request->route('id'));
        $classInvitePurpose = 11;

        //check if there is already a pending invite
        if($touser->notifications()
                ->where('user_id', '=', $touser->id)
                ->where('from_user_id', '=', $fromuser->id)
                ->where('purpose', '=', $classInvitePurpose)
                ->exists() || $fromuser->notifications()
                ->where('user_id', '=', $fromuser->id)
                ->where('from_user_id', '=', $touser->id)
                ->where('purpose', '=', $classInvitePurpose)
                ->exists())
        {
            $classInviteExists = true;
        }
        else
        {
            $classInviteExists = false;
        }

        //check if the student is already a part of the classroom
        if($classroom->students->contains($touser))
        {
            return redirect()->back()->with('error', 'That person is already in this classroom.');
        }
        elseif($classInviteExists)
        {
            return redirect()->back()->with('error', 'That person has a pending invite to this classroom.');
        }
        elseif(!$touser->classes->contains($classroom))
        {
            Notification::send($fromuser->id, $touser->id, $classInvitePurpose, $classroom->name, $classroom->id);
            return redirect()->back()->with('success', 'Classroom invite sent.');
        }
    }

    public function acceptInvite($fromuser, $classroomid)
    {
        $classInvitePurpose = 11;

        $classInvite = Notification::where('from_user_id', '=', $fromuser)->where('user_id', '=', Auth::user()->id)
            ->where('purpose', '=', $classInvitePurpose);

        Auth::user()->classes()->attach($classroomid);

        $classInvite->delete();

        return redirect()->back();
    }

    public function declineInvite($fromuser, $classroomid)
    {
        $classInvitePurpose = 11;

        $classInvite = Notification::where('from_user_id', '=', $fromuser)->where('user_id', '=', Auth::user()->id)
            ->where('purpose', '=', $classInvitePurpose);

        $classInvite->delete();

        return redirect()->back();
    }

    public function requestAccess(Request $request)
    {
        $classroom = Classroom::where('join_code', '=', $request->input('classcode'))->first();
        $touser = User::find($classroom->teacher_id);
        $fromuser = Auth::user();
        $classRequestPurpose = 12;

        //check if there is already a pending invite
        if($touser->notifications()
                ->where('user_id', '=', $touser->id)
                ->where('from_user_id', '=', $fromuser->id)
                ->where('purpose', '=', $classRequestPurpose)
                ->exists() || $fromuser->notifications()
                ->where('user_id', '=', $fromuser->id)
                ->where('from_user_id', '=', $touser->id)
                ->where('purpose', '=', $classRequestPurpose)
                ->exists())
        {
            $classRequestExists = true;
        }
        else
        {
            $classRequestExists = false;
        }

        //check if the student is already a part of the classroom
        if($classroom->students->contains($fromuser))
        {
            return redirect()->back()->with('error', 'You are already a student of that class.');
        }
        elseif($classroom->teacher_id == $fromuser->id)
        {
            return redirect()->back()->with('error', 'You are the teacher of that class.');
        }
        elseif($classRequestExists)
        {
            return redirect()->back()->with('error', 'You have a pending request for that class.');
        }
        elseif(!$touser->classes->contains($classroom))
        {
            Notification::send($fromuser->id, $touser->id, $classRequestPurpose, $classroom->name, $classroom->id);
            return redirect()->back()->with('success', 'Classroom Request sent.');
        }
    }

    public function acceptRequest($fromuserid, $classid)
    {
        $classRequestPurpose = 12;
        $classroom = Classroom::find($classid);

        $classInvite = Notification::where('from_user_id', '=', $fromuserid)->where('user_id', '=', Auth::user()->id)
            ->where('purpose', '=', $classRequestPurpose);

        $classroom->students()->attach($fromuserid);

        $classInvite->delete();

        return redirect()->back()->with('success', 'Student added to classroom successfully.');
    }

    public function declineRequest($fromuserid, $classid)
    {
        $classRequestPurpose = 12;

        $classInvite = Notification::where('from_user_id', '=', $fromuserid)->where('user_id', '=', Auth::user()->id)
            ->where('purpose', '=', $classRequestPurpose);

        $classInvite->delete();

        return redirect()->back()->with('success', 'Student request denied.');
    }

    public function leaveClassroom($classroomid)
    {
        Auth::user()->classes()->detatch($classroomid);
        return redirect()->back();
    }

    public function query()
    {
        $classes = DB::table('classrooms')->select('id', 'teacher_id', 'header_image', 'name', 'join_code')->get();

        foreach($classes as $class)
        {
            $classArray[] = ['teacher_id' => $class->teacher_id, 'name' => $class->name, 'joincode' => $class->join_code, 'image' => '/images/classroom/' . $class->id . '/header'];
        }

        return response()->json($classArray);
    }
}
