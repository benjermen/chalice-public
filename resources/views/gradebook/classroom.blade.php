@extends('layouts.gradebook')

@section('body')
    @if(count($classroom->assignments) > 0)
        <table>
            <tr>
                <th>Student</th>
                @foreach($classroom->assignments as $assignment)
                    <th><a href="/assignment/{{ $assignment->id }}">{{ $assignment->name }}</a></th>
                @endforeach
            </tr>
            @foreach($classroom->students as $student)
                <tr>
                    <td><a href="/user/{{ $student->id }}">{{ $student->fullName() }}</a></td>
                    @foreach($classroom->assignments as $assignment)
                        @foreach($assignment->grades as $grade)
                            @if($grade->user_id == $student->id)
                                <td>{{ $grade->percentage }} ({{ $grade->letterGrade() }})</td>
                            @endif
                        @endforeach
                    @endforeach
                </tr>
            @endforeach
        </table>
    @else
        <div class="text-center">
            You have no assignments in this class.
        </div>
    @endif
@stop