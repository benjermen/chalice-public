@extends('layouts.app')

@section('body')

    <div class="col-md-6">
        <div class="create-chapter">
            <h2>Create Chapter</h2>

            <form action="/create/chapter" method="post">
                {{ csrf_field() }}

                <br>

                <div class="form-group">
                    <label for="class-select">Apply to classroom:</label>
                    <select name="classroom-select" id="class-select" class="form-control">
                        @foreach($classrooms as $classroom)
                            <option value="{{ $classroom->id }}" @if(Request::old('classroom-select') == $classroom->id) selected @endif>{{ $classroom->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="chap-name">Name</label>
                    <input type="text" id="chap-name" class="form-control" name="chapter-name" placeholder="Chapter 1" value="{{ Request::old('chapter-name') }}">
                </div>

                <div class="form-group">
                    <label for="chap-descrip">Description</label>
                    <textarea name="chapter-description" id="chap-descrip" cols="30" rows="10"
                              placeholder="Describe this lesson set..." class="form-control">{{ Request::old('chapter-description') }}</textarea>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Create Chapter!">
                </div>
            </form>
            @if(strlen($errors->chapterErrors->first()) > 0)
                <div class="alert alert-danger">
                    {{ $errors->chapterErrors->first() }}
                </div>
            @endif
            @if(Session::has('chapterSuccess'))
                <div class="alert alert-success">
                    {{ Session::get('chapterSuccess') }}
                </div>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="create-lesson">
            <h2>Create Lesson</h2>

            <form action="/create/lesson" method="post">
                {{csrf_field()}}

                <br>

                <div class="form-group">
                    <label for="chap-select">Apply to chapter:</label>
                    <select class="form-control" name="chapter-select" id="chap-select">
                        @foreach($classrooms as $classroom)
                            <option value="" disabled>--- {{ $classroom->name }} ---</option>
                            @foreach($classroom->chapters as $chapter)

                                <option value="{{ $chapter->id }}" @if(Request::old('chapter-select') == $chapter->id) selected  @endif>{{ $chapter->name }}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="lesson-name" class="control-label">Name</label>
                    <input type="text" class="form-control" id="lesson-name" name="lesson_name" placeholder="Addition & Subtraction..." value="{{ Request::old('lesson_name') }}">
                </div>
                @if(strlen($errors->lessoncreate->first('lesson_name')) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->lessoncreate->first('lesson_name') }}
                    </div>
                @endif
                <div class="form-group">
                    <label for="video-url" class="control-label">YouTube URL</label>
                    <input type="text" class="form-control" name="videourl" id="video-url" placeholder="https://www.youtube.com/watch?v=......" value="{{ Request::old('videourl') }}">
                </div>
                @if(strlen($errors->lessoncreate->first('videourl')) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->lessoncreate->first('videourl') }}
                    </div>
                @endif
                <div class="form-group">
                    <label for="lesson-description" class="control-label">Description</label>
                    <textarea name="lesson_description" id="lesson-description" cols="30" rows="10"
                              class="form-control" placeholder="Describe this lesson...">{{ Request::old('lesson_description') }}</textarea>
                </div>
                @if(strlen($errors->lessoncreate->first('lesson_description')) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->lessoncreate->first('lesson_description') }}
                    </div>
                @endif
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="comments" @if(Request::old('comments') == 'on') checked @endif> Enable Comments on Lesson Page
                    </label>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Create Lesson!">
                </div>

                @if(Session::has('lesson-success'))
                    <div class="alert alert-success">
                        {{ Session::get('lesson-success') }}
                    </div>
                @endif
            </form>
        </div>
    </div>
@stop