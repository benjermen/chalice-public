<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use Carbon\Carbon;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\User as User;
use App\Subject as Subject;
use App\Test;
use App\Assignment;
use App\Question;

function randomArray($min, $max, $quantity)
{
	$numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

function generateRandString($length) {
    $randString = '';
    $chars = '0123456789ab(cdef&ghijklm.no*$pqrstu)vwxyzAB$CDEF!G@HIJ$KLMNOPQ^RSTUVWXYZ';
    $charsLength = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $randString .= $chars[rand(0, $charsLength - 1)];
    }
    return $randString;
}

function generateRandAlpha($length) {
    $randString = '';
    $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charsLength = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $randString .= $chars[rand(0, $charsLength - 1)];
    }
    return $randString;
}


$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'privilege' => $faker->randomElement($array = array ('0','1','2')),
        'socket' => generateRandString(16),
        'invite_id' => generateRandAlpha(6),
        'remember_token' => str_random(10),
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Classroom::class, function (Faker\Generator $faker){
	return [
        'teacher_id' => 3,
        'name' => $faker->word,
		'description' => $faker->text,
        'join_code' => generateRandAlpha(8),
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
	];
});

$factory->define(App\Assignment::class, function (Faker\Generator $faker){
    return [
        'classroom_id' => 0,
        'name' => $faker->word,
        'description' => $faker->text,
        'test_id' => null,
        'file_name' => null,
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Grade::class, function (Faker\Generator $faker){
    return [
        'user_id' => 0,
        'assignment_id' => 0,
        'percentage' => $faker->randomFloat(2, 39.99, 100.00),
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Attempt::class, function (Faker\Generator $faker){
    $userids = User::lists('id')->all();
    $ansJson = array();

    for($i = 0; $i < 20; $i++)
    {
        $ansJson[($i + 1)] = $faker->randomElement(array('a', 'b', 'c', 'd'));
    }

    $ansJson = json_encode($ansJson);

    return [
        'user_id' => $faker->randomElement($userids),
        'test_id' => $faker->numberBetween(1, 10),
        'answers' => $ansJson,
    ];
});

$factory->define(App\Conversation::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->word,
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Test::class, function (Faker\Generator $faker) {
    $userids = User::lists('id')->all();
    $assignmentids = Assignment::lists('id')->all();

    return [
        'creator_id' => $faker->randomElement($userids),
        'name' => $faker->word,
        'description' => $faker->text,
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Chapter::class, function (Faker\Generator $faker) {
    return [
        'course_id' => null,
        'classroom_id' => null,
        'description' => $faker->text,
        'name' => $faker->word,
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});

$factory->define(App\Question::class, function (Faker\Generator $faker) {
    $testid = $faker->randomElement(Test::lists('id')->all());
    $setid = Question::where('test_id', '=', $testid)->max('set_id') + 1;

    return [
        'test_id' => $testid,
        'set_id' => $setid,
        't_f' => 0,
        'question' => $faker->sentence(10),
        'answer' => $faker->randomElement(array('a', 'b', 'c', 'd')),
        'ans_a' => $faker->sentence(8),
        'ans_b' => $faker->sentence(8),
        'ans_c' => $faker->sentence(8),
        'ans_d' => $faker->sentence(8),
        'created_at' => Carbon::now('America/Detroit'),
        'updated_at' => Carbon::now('America/Detroit'),
    ];
});