<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function(Blueprint $table){
            $table->increments('id');
            $table->integer('subject_id')->unsigned();
            $table->string('join_code')->nullable();
            $table->string('img');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });

        Schema::table('courses', function(Blueprint $table){
            $table->foreign('subject_id')->references('id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('courses');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
