@extends('layouts.app')

@section('body')

    <div class="col-md-6">
        <div class="create-chapter">
            <h2>Create Chapter Set</h2>

            <br>

            <form action="/create/chapterset" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="chapterset-name">Name</label>
                    <input type="text" class="form-control" id="chapterset-name" name="name" placeholder="English Semester 1">
                </div>
                <div class="form-group">
                    <label for="chapterset-description">Description</label>
                    <textarea name="description" id="chapterset-description" cols="30" rows="10"
                              class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Create Chapter Set.">
                </div>
            </form>
        </div>
    </div>
@stop