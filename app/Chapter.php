<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters';

    public function courses()
    {
        return $this->belongsTo('App\Courses');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function chapterset()
    {
        return $this->belongsTo('App\ChapterSet');
    }
}
