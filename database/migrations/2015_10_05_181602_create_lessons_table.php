<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function(Blueprint $table){
            $table->increments('id');
            $table->integer('chapter_id')->unsigned()->nullable();
            $table->boolean('has_comments');
            $table->string('video_url');
            $table->string('description');
            $table->string('name');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('lessons');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
