<?php

use Illuminate\Database\Seeder;
use App\Course as Course;
use Carbon\Carbon;
use App\Forum;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->forums();
    }

    public function forums()
    {
        $forums = [

            'Algebra Basics',
            'Algebra I',
            'Algebra II',
            'Pre-Calculus',
            'Calculus',
            'Physics',
            'Chemistry',
            'Biology',
            'American History',
            'World History',
            'Microeconomics',
            'Macroeconomics',
            'Computer Programming with C++',
            'Computer Programming with Java',
            'Web Development',
            'Computer Science',
            'SAT',
            'MCAT',
        ];

        foreach ($forums as $forum) {
            $forums1 = new Forum;
            $forums1->title = $forum;
            $forums1->created_at = Carbon::now('America/Detroit');
            $forums1->updated_at = Carbon::now('America/Detroit');
            $forums1->save();
        }
    }
}