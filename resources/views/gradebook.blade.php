@extends('layouts.gradebook')

@section('body')
    @if(Auth::user()->teacher() && Auth::user()->id == $class->teacher_id)
        @if(count($classrooms) > 0)
            @if(count($classroom->assignments) > 0)
                <table>
                    <tr>
                        <th>Student</th>
                        @foreach($classroom->assignments as $assignment)
                            <th><a href="/assignment/{{ $assignment->id }}">{{ $assignment->name }}</a></th>
                        @endforeach
                    </tr>
                    @foreach($classroom->students as $student)
                        <tr>
                            <td><a href="/user/{{ $student->id }}">{{ $student->fullName() }}</a></td>
                            @foreach($classroom->assignments as $assignment)
                                @foreach($assignment->grades as $grade)
                                    @if($grade->user_id == $student->id)
                                        <td>{{ $grade->percentage }} ({{ $grade->letterGrade() }})</td>
                                    @endif
                                @endforeach
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            @else
                <div class="text-center">
                    You have no assignments in this class.
                </div>
            @endif
        @else
            <div class="text-center">
                No selected classroom.
            </div>
        @endif
    @else
        @if(isset($classrooms))
            @if(count($class->assignments) > 0)
                <table>
                    <tr>
                        @foreach($class->assignments as $assignment)
                            <th><a href="/assignment/{{ $assignment->id }}">{{ $assignment->name }}</a></th>
                        @endforeach
                    </tr>

                    <tr>
                        @foreach($class->assignments as $assignment)
                            @foreach($assignment->grades as $grade)
                                @if($grade->user_id == Auth::user()->id)
                                    <td>{{ $grade->percentage }} ({{ $grade->letterGrade() }})</td>
                                @endif
                            @endforeach
                        @endforeach
                    </tr>
                </table>
            @else
                <div class="text-center">
                    You have no assignments in this class.
                </div>
            @endif
        @endif
    @endif
@stop