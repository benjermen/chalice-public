<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'privilege'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function attempts()
    {
        return $this->hasMany('App\Attempt', 'user_id', 'id');
    }

    /*
     * User must have answered all questions in the test in order for it to count as an attempt.
     */
    public function attemptCount($id)
    {
        $attempts = $this->attempts()->where('test_id', '=', $id)->get();
        $attemptCount = 0;
        $bool = false;

        foreach ($attempts as $attempt) {
            $arrayAttempt = json_decode($attempt);
            foreach ($arrayAttempt as $questionNumber => $answer) {
                if ($answer == '') {
                    $bool = false;
                    break;
                } else {
                    $bool = true;
                }
            }

            if ($bool) {
                $attemptCount++;
            }
        }

        return $attemptCount;
    }

    public function latestAttempt($id)
    {
        $attempts = $this->attempts()->where('test_id', '=', $id)->get();
        $test = Test::find($id);

        if (count($attempts) == 0) {
            $answers = json_encode(array_fill(1, (count($test->questions)), ''));

            $attempt = new Attempt;
            $attempt->user_id = Auth::user()->id;
            $attempt->test_id = $id;
            $attempt->answers = $answers;
            $attempt->save();

            return $attempt->id;
        }

        $bool = true;

        foreach ($attempts as $attempt) {
            $arrayAttempt = json_decode($attempt);
            foreach ($arrayAttempt as $questionNumber => $answer) {
                if ($answer == '') {
                    $latest = $attempt->id;
                    $bool = false;
                    break;
                }
            }

            if ($bool) {
                $latest = $attempt->id;
            }
        }

        return $latest;
    }

    public function chaptersets()
    {
        return $this->hasMany('App\ChapterSet', 'creator_id', 'id');
    }

    /*
     * Classes that the user is teaching
     */
    public function classrooms()
    {
        return $this->hasMany('App\Classroom', 'teacher_id', 'id');
    }

    /*
     * Classrooms that the user are a student in
     */
    public function classes()
    {
        return $this->belongsToMany('App\Classroom', 'classroom_user', 'user_id', 'classroom_id');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_user', 'user_id', 'course_id');
    }

    public function conversations()
    {
        return $this->belongsToMany('App\Conversation', 'conversation_user', 'user_id', 'conversation_id');
    }

    public function friends()
    {
        return $this->belongsToMany('App\User', 'user_friends', 'user_id', 'friend_id');
    }

    public function getOnlineFriends()
    {
        $onlineFriends = $this->friends()->get()->where('is_online', 1);

        return $onlineFriends;
    }

    public function grades()
    {
        return $this->hasMany('App\Grade', 'user_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'user_id', 'id');
    }

    public function personality()
    {
        return $this->hasOne('App\Personality', 'user_id', 'id');
    }

    public function tests()
    {
        return $this->hasMany('App\Test', 'creator_id', 'id');
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function teacher()
    {
        if ($this->privilege == 1 || $this->privilege == 5 || $this->privilege == 9) {
            return true;
        }

        return false;
    }

    public static function addCourse($courseid)
    {
        $user = Auth::user();
        $user->courses()->attach($courseid);
    }

    public static function courseCheck($courseid)
    {
        $user = Auth::user();

        $blah = $user->courses()->wherePivot('course_id', '=', $courseid)->get();

        if (count($blah) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addFriend($userid)
    {
        $user = Auth::user();

        if (!$user->friends()->contains($userid)) {
            $user->friends()->attach($userid);
        }
    }

    public function privateConversations()
    {
        $conversationusers = $this->with('conversations.users')->where('user_id', '=', Auth::user()->id);

        $userCount = 0;

        foreach ($conversationusers->conversations->users as $user) {
            $userCount++;
        }

        return $userCount;
    }

    public function getProfileImage()
    {
        if ($this->profile_image != "" && \Storage::disk(ENV('STORAGE_DISK'))->has('/img/users/' . $this->id . '/' . $this->profile_image)) {
            return '/img/users/' . $this->id . '/' . $this->profile_image;
        }

        return '/img/default.png';
    }

    public function getHeaderImage()
    {
        if ($this->header_image != "" && \Storage::disk(ENV('STORAGE_DISK'))->has('/img/users/' . $this->id . '/header/' . $this->header_image)) {
            return '/img/users/' . $this->id . '/header/' . $this->header_image;
        }

        return '/img/defaultheader.jpg';
    }

    public static function generateInviteId()
    {
        $randString = '';
        $length = 6;
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charsLength = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $randString .= $chars[rand(0, $charsLength - 1)];
        }
        return $randString;

    }
}
