@extends('layouts.app')

@section('body')
    <div class="container-fluid">
        <div class="row">
            @foreach($courses as $course)
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <a href="/learn/{{ $subject_name }}/{{ $course->name }}" class="pane btn-block">
                        <div class="pane-image">
                            <img src="{{ $course->img }}" alt="">
                        </div>
                        <div class="pane-text">
                            <div class="pane-text-main">{{ $course->name }}</div>
                            <div class="pane-text-sub">View Chapters & Lessons</div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@stop
