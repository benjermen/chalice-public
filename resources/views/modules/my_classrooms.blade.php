@if(Auth::user()->teacher())
    @foreach(Auth::user()->classrooms()->get() as $classroom)
        <div class="col-md-12">
            <a href="/classroom/{{ $classroom->id }}" class="pane btn-block">
                <div class="pane-image">
                    <img src="/images/classroom/{{ $classroom->id }}/header" alt="">
                </div>
                <div class="pane-text">
                    <div class="pane-text-main">{{ $classroom->name }}</div>
                    <div class="pane-text-sub">View Classroom</div>
                </div>
            </a>
        </div>
    @endforeach
    @foreach(Auth::user()->classes()->get() as $classroom)
        <div class="col-md-12">
            <a href="/classroom/{{ $classroom->id }}" class="pane btn-block">
                <div class="pane-image">
                    <img src="/images/classroom/{{ $classroom->id }}/header" alt="">
                </div>
                <div class="pane-text">
                    <div class="pane-text-main">{{ $classroom->name }}</div>
                    <div class="pane-text-sub">View Classroom</div>
                </div>
            </a>
        </div>
    @endforeach
@else
    @foreach(Auth::user()->classes()->get() as $classroom)
        <div class="col-md-12">
            <a href="/classroom/{{ $classroom->id }}" class="pane btn-block">
                <div class="pane-image">
                    <img src="/images/classroom/{{ $classroom->id }}/header" alt="">
                </div>
                <div class="pane-text">
                    <div class="pane-text-main">{{ $classroom->name }}</div>
                    <div class="pane-text-sub">View Classroom</div>
                </div>
            </a>
        </div>
    @endforeach
@endif
