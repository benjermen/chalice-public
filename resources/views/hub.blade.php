@extends('layouts.hub')


@section('body')
    <div class="row header-tile" style="background:url('/images/user/{{ Auth::user()->id }}/header') no-repeat 0 0;">
        <div class="m-a-0 row">
            <div class="col-md-6" style="margin-left: 25%;">
                <img src="/images/user/{{ Auth::user()->id }}" alt="" class="profile-img">
            </div>
            <div class="col-md-3 img-upload">
                <form action="/user/updateheader/{{ Auth::user()->id }}" id="header-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <span class="btn btn-outline-secondary btn-file btn-sm">
                        Header Image <input type="file" name="image" id="header-image-input">
                    </span>
                </form>
                @if(strlen($errors->headerimg->first('image')) > 0)
                    <div class="alert alert-danger p-t-1">
                        {{ $errors->headerimg->first('image') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="m-a-0 row wrapper-name">
            <h2 class="fullname">{{ Auth::user()->fullName() }}</h2>
        </div>
    </div>

    <div class="hub-column col-lg-4 col-md-6 col-sm-12">
        <div class="row heading-center">
            <h2 class="text-center">My Classrooms</h2>
            <button type="button" class="small-icon btn-small-plus fa fa-plus" id="btn-class-search" data-toggle="modal" data-target="#class-req-modal"></button>
        </div>

        <div class="modal fade" id="class-req-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Request Classroom Access</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/classroom/requestaccess" method="post">
                            {{ csrf_field() }}
                            <div class="form-group" id="tt-class-request">
                                <label for="name">Class Code</label>
                                <input type="text" placeholder="Class Name" class="form-control typeahead" name="classcode">
                            </div>

                            <input type="submit" class="btn btn-primary" value="Request Access">

                            <div class="form-group m-t-1">
                                <div class="alert alert-info">
                                    You may also retrieve the 8-digit class code from your teacher and manually enter it.
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        @if(count(Auth::user()->classrooms()->get()) == 0 && count(Auth::user()->classes()->get()) == 0)
            <div class="col-sm-12 blank-panel">
                <h5>No classrooms yet!</h5>
            </div>
        @endif

        @include('modules/my_classrooms')
    </div>

    <div class="hub-column col-lg-4 col-md-6 col-sm-12">
        <div class="row heading-center">
            <h2>Recent Courses</h2>
        </div>
        @if(count(Auth::user()->courses()->get()) == 0)
            <a href="/learn">
                <div class="col-sm-12 blank-panel">
                    <h5>Start Learning!</h5>
                </div>
            </a>
        @endif
        @foreach(Auth::user()->courses()->get() as $course)
            <div class="col-md-12">
                <a href="/learn/{{ $course->subjects->name }}/{{ $course->name }}" class="pane btn-block">
                    <div class="pane-image">
                        <img src="{{ $course->img }}" alt="">
                    </div>
                    <div class="pane-text">
                        <div class="pane-text-main">{{ $course->name }}</div>
                        <div class="pane-text-sub">View Chapters & Lessons</div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>

    <div class="hub-column col-lg-4 col-md-6 col-sm-12">
        <div class="row heading-center">
            <h2>Online Friends</h2>
        </div>
        @if(count(App\User::find(Auth::user()->id)->getOnlineFriends()) == 0)
            <div class="col-sm-12 blank-panel">
                <h5>No online friends!</h5>
            </div>
        @endif

        @include('modules/friends_list')
    </div>

@stop
