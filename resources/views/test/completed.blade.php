@extends('layouts.app')

@section('body')
    <div class="container">


        
        <div class="m-t-1">
            <form action="/autograde/attempt/{{ $attempt->id }}" method="post">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-primary" value="Submit & Grade Test">
            </form>
        </div>
    </div>
@stop