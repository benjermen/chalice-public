@extends('layouts.app')

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-xs-12">
                <a href="/create/chapter" class="pane btn-block">
                    <div class="pane-image">
                        <img src="/img/lesson.jpg" alt="">
                    </div>
                    <div class="pane-text">
                        <div class="pane-text-main">Create a Chapter</div>
                        <div class="pane-text-sub">Create a lesson set for your students.</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <a href="/create/test" class="pane btn-block">
                    <div class="pane-image">
                        <img src="/img/test.jpg" alt="">
                    </div>
                    <div class="pane-text">
                        <div class="pane-text-main">Create a Test</div>
                        <div class="pane-text-sub">Create a test for your students to learn from.</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <a href="/create/classroom" class="pane btn-block">
                    <div class="pane-image">
                        <img src="/img/classroom.jpg" alt="">
                    </div>
                    <div class="pane-text">
                        <div class="pane-text-main">Create a Classroom</div>
                        <div class="pane-text-sub">Create a classroom for your students to join.</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <a href="/create/assignment" class="pane btn-block">
                    <div class="pane-image">
                        <img src="/img/assignment.jpg" alt="">
                    </div>
                    <div class="pane-text">
                        <div class="pane-text-main">Create an Assignment</div>
                        <div class="pane-text-sub">Create an assignment for your students to be graded on.</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@stop