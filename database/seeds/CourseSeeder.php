<?php

use Illuminate\Database\Seeder;
use App\Course as Course;
use Carbon\Carbon;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->courses();
    }

    public function courses()
    {
    	//Initialize faker
    	$faker = Faker\Factory::create();

    	$subjectCourses = [
	    	'Math' => [
	    		'Algebra Basics',
	    		'Algebra I',
	    		'Algebra II',
	    		'Pre-Calculus',
	    		'Calculus',
	    	],

	    	'Science' => [
	    		'Physics',
	    		'Chemistry',
	    		'Biology',
	    	],

	    	'History' => [
	    		'American History',
	    		'World History',
	    	],

	    	'Economics' => [
	    		'Microeconomics',
	    		'Macroeconomics',
	    	],

	    	'Computing' => [
	    		'Computer Programming with C++',
	    		'Computer Programming with Java',
	    		'Web Development',
	    		'Computer Science',
	    	],

	    	'Test Prep' => [
	    		'SAT',
	    		'MCAT',
	    	],
    	];



    	foreach($subjectCourses as $subjectName => $subject)
    	{
    		$subjectid = DB::table('subjects')->where('name', '=', $subjectName)->value('id');
			$subjectNameLow = preg_replace('/\s*/', '', $subjectName);
			$subjectNameLow = strtolower($subjectNameLow);

    		foreach($subject as $course)
    		{
    			$course1 = new Course;
	    		$course1->subject_id = $subjectid;
	    		$course1->description = $faker->text;
				$course1->join_code = str_random(6);
                $course1->img = '/img/courses/' . $subjectNameLow . '/' . str_replace('+', '', preg_replace('/\s*/', '', strtolower($course))) . '.jpg';
	    		$course1->name = $course;
	    		$course1->created_at = Carbon::now('America/Detroit');
	        	$course1->updated_at = Carbon::now('America/Detroit');
	        	$course1->save();
    		}
    	}
    }
}

