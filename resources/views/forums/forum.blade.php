@extends('layouts.app')

@section('body')
    <div class="col-md-12">
        <h1>{{$forums->title}}</h1>
    <div class="row full-height">
        <div class="p-l-0 full-height fixed-sidebar">
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="/createthread/{{$forums->id}}"><input type="submit" class="btn btn-primary btn-block" value="Create a Thread"></a>
            </div>
            <div class="col-sm-4">
                <input type="submit" class="btn btn-primary btn-block" value="My Forums">
            </div>
        </div>
        <br>
        @foreach($forums->threads as $thread)
            <ul class="list-unstyled forum-page list-group">
                <li class="list-group-item" style="margin-bottom: 3px;">
                        <a href="/forums/forum/thread/{{$thread->id}}">
                           <h4> <span>{{ $thread->title}}</span></h4>
                        </a>
                </li>
            </ul>
        @endforeach
    </div>
@stop