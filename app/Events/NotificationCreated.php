<?php

namespace App\Events;

use App\Notification;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationCreated extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $notification;
    public $channel;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification $notif, $channel)
    {
        $this->notification = $notif;
        $this->channel = $channel;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['notifAction'];
    }
}
