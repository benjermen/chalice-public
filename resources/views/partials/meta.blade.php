<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="_token" content="{{ csrf_token() }}">
<meta name="channel" content="{{ \Auth::check() ? \Auth::user()->socket : "" }}">
<title>Chalice</title>

<link rel="stylesheet" href="{{ URL::asset('css/chalice.css') }}">