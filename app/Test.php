<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Test extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assignments()
    {
        return $this->belongsToMany('App\Assignment', 'assignment_test', 'test_id', 'assignment_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'test_id', 'id');
    }

    public static function getName($id)
    {
        return Test::find($id)->value('name');
    }
}
