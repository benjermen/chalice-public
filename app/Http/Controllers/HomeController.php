<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Personality;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use Validator;
use Auth;
use App\User as User;

$GLOBALS['teacher'] = false;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /*
     * Run the request through validation, then attempt login
     *
     * @param Request $request
     * @return redirect
     */
    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];

        $messages = [
            'required' => 'The :attribute field is required.',
            'email' => 'The email field must contain a email.',
            'password:min' => 'Your :attribute must be greater than 6 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        /* Validating that there is input for email and password, and reporting back with errors if not */
        if ($validator->fails()) {
            return redirect('/auth/login')
                ->withErrors($validator->messages(), 'login');
        }

        /* Does the user exist ? If so go to hub */
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect('/hub');
        }

        if (!Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect('/auth/login')
                ->withErrors('Invalid email/password combination.', 'general');
        }
    }

    /*
     * Run the request through validation, and make the user if they pass through
     *
     * @param Request $request
     * @return redirect
     */
    public function register(Request $request)
    {
        /* Validator rules, checks email in database to make sure it isn't there already */
        $rules = [
            'email' => 'required|email|max:255|unique:users,email',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        ];

        /* Custom error messages so the user knows what they missed */
        $messages = [
            'required' => 'The :attribute field is required.',
            'email' => 'The email field must contain a email.',
            'password:min' => 'Your :attribute must be greater than 6 characters.',
            'max' => 'The :attribute field may only contain 255 characters.',
            'same' => 'The passwords do not match.',
            'unique' => 'There is already a Chalice account with that email.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        /* Replying back with error messages */
        if ($validator->fails()) {
            return redirect('/auth/register')
                ->withErrors($validator->messages(), 'signup');
        }

        $userArray = $request->all();

        $userArray['teacher'] = $request->input('teacher');

        Auth::login($this->create($userArray));

        $user = Auth::user();

        if ($GLOBALS['teacher']) {
            Notification::sendModIssue($user->id);
        }

        Mail::send('emails.register', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email);
            $m->subject('Welcome to Chalice!');
        });

        return redirect('/hub');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        function generateRandString($length)
        {
            $randString = '';
            $chars = '0123456789ab(cdef&ghijklm.no*$pqrstu)vwxyzAB$CDEF!G@HIJ$KLMNOPQ^RSTUVWXYZ';
            $charsLength = strlen($chars);
            for ($i = 0; $i < $length; $i++) {
                $randString .= $chars[rand(0, $charsLength - 1)];
            }
            return $randString;
        }

        function generateRandAlpha($length) {
            $randString = '';
            $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charsLength = strlen($chars);
            for ($i = 0; $i < $length; $i++) {
                $randString .= $chars[rand(0, $charsLength - 1)];
            }
            return $randString;
        }

        $privilege = 0;

        if ($data['teacher'] == 'on') {
            $GLOBALS['teacher'] = true;
            $privilege = 9;
        }

        $user = new User;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->socket = generateRandString(16);
        $user->invite_id = generateRandAlpha(6);
        $user->privilege = $privilege;
        $user->save();




        $personality = new Personality;
        $personality->user_id = $user->id;
        $personality->save();

        return $user;
    }


}

