<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;

class ManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage')
            ->with('classrooms', Auth::user()->classrooms()->get())
            ->with('chaptersets', Auth::user()->chaptersets()->get())
            ->with('tests', Auth::user()->tests()->get());
    }

    public function archives()
    {
        return view('manage.archives')
            ->with('classrooms', Auth::user()->classrooms()->withTrashed()->get())
            ->with('chaptersets', Auth::user()->chaptersets()->withTrashed()->get())
            ->with('tests', Auth::user()->tests()->withTrashed()->get());
    }
}
