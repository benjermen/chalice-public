@extends('layouts.app')

@section('body')
    <div class="container">
        <div class="row m-b-2 text-center">
            <h4>{{ \App\Test::getName($attempt->test_id) }}</h4>
        </div>

        <div class="row">
            <p class="question"><strong>{{ $question->set_id }}.</strong> {{ $question->question }}</p>
        </div>

        <div class="row">
            <form action="/attempt/{{ $attempt->id }}/question/{{ $question->id }}" method="post">
                {{ csrf_field() }}
                @if($question->t_f)
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer1" value="a">
                            <span class="m-l-1"><strong>True</strong></span>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer2" value="b">
                            <span class="m-l-1"><strong>False</strong></span>
                        </label>
                    </div>
                @else
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer1" value="a">
                            <span class="m-r-1"><strong>A.</strong></span> {{ $question->ans_a }}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer2" value="b">
                            <span class="m-r-1"><strong>B.</strong></span> {{ $question->ans_b }}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer3" value="c">
                            <span class="m-r-1"><strong>C.</strong></span> {{ $question->ans_c }}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="answer" id="answer4" value="d">
                            <span class="m-r-1"><strong>D.</strong></span> {{ $question->ans_d }}
                        </label>
                    </div>
                @endif


                <div class="col-md-4 m-b-1">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
            </form>

            <div class="col-md-3 push-md-5">
                <form action="/attempt/{{ $attempt->id }}/question/{{ $question->id }}/skip" method="post">
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-outline-primary" value="Skip and Come Back">
                </form>
            </div>
        </div>
    </div>
@stop