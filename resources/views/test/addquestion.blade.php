@extends('layouts.app')

@section('body')
    <div class="row text-center m-b-2">
        <h3>{{ $test->name }}</h3>
    </div>

    <div class="col-md-6 m-b-3 border-top p-t-1">

        <form action="/test/{{ $test->id }}/edit/addquestion" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="t-f" name="t-f" onclick="hideAns(this)"> True or False
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label for="quest">Question</label>
                <textarea name="question" class="form-control" id="quest" cols="30" rows="5" placeholder="What is 2+2?">{{ Request::old('question') }}</textarea>
            </div>
            <div id="ans">
                <div class="form-group">
                    <label for="answer-a">Answer A:</label>
                    <input type="text" name="ans-a" id="answer-a" class="form-control"
                           value="{{ Request::old('ans-a') }}">
                </div>
                <div class="form-group">
                    <label for="answer-b">Answer B:</label>
                    <input type="text" name="ans-b" id="answer-b" class="form-control"
                           value="{{ Request::old('ans-b') }}">
                </div>
                <div class="form-group">
                    <label for="answer-c">Answer C:</label>
                    <input type="text" name="ans-c" id="answer-c" class="form-control"
                           value="{{ Request::old('ans-c') }}">
                </div>
                <div class="form-group">
                    <label for="answer-d">Answer D:</label>
                    <input type="text" name="ans-d" id="answer-d" class="form-control"
                           value="{{ Request::old('ans-d') }}">
                </div>
            </div>

            <br>
            <h5>Which is correct?</h5>
            <br>

            <div class="full-width m-b-1">
                <div class="col-xs-3 t-f">
                    <div class="radio">
                        <label>
                            <input type="radio" name="correct" id="optionsRadios1" value="a" checked>
                            Answer A (true)
                        </label>
                    </div>
                </div>
                <div class="col-xs-3 t-f">
                    <div class="radio">
                        <label>
                            <input type="radio" name="correct" id="optionsRadios2" value="b">
                            Answer B (false)
                        </label>
                    </div>
                </div>
                <div id="c-d">
                    <div class="col-xs-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="correct" id="optionsRadios3" value="c">
                                Answer C
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="correct" id="optionsRadios4" value="d">
                                Answer D
                            </label>
                        </div>
                    </div>
                </div>
            </div>


            <input type="submit" class="btn btn-primary btn-block" value="Add Question">

            @if(strlen($errors->questionErrors->first()) > 0)
                <div class="alert alert-danger">
                    {{ $errors->questionErrors->first() }}
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
        </form>

    </div>

    <div class="col-md-6">
        @include('partials.question-list')
    </div>
@stop