<?php

namespace App\Http\Controllers;

use App\Assignment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attempt;
use App\Test;
use App\Question;
use App\Classroom;
use Auth;
use Illuminate\Support\Collection;
use App\Grade;

class AttemptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function newAttempt($testid, $assignmentid)
    {
        //Grab all classroom ids from assignments that hold this test
        $classids = Test::find($testid)->assignments()->get()->lists('classroom_id');
        $classrooms = collect();

        //Grab each classroom and add it to a collection
        foreach($classids as $classid)
        {
            $classrooms = $classrooms->push(Classroom::with('students')->find($classid));
        }

        $user = Auth::user();

        foreach($classrooms as $classroom)
        {
            //Make sure the user is the teacher of the class that the test is in, else, check if they are a student of the class.
            if($user->id == $classroom->teacher_id)
            {
                //Check if the user already has a non-finished test, if not, make new attempt, if so, then go to the latest question that they do not have answered.
                if(Attempt::attemptAuth($testid))
                {
                    $test = Test::with('questions')->find($testid);

                    //Make a new attempt for this test
                    $attempt = new Attempt;
                    $attempt->user_id = Auth::user()->id;
                    $attempt->test_id = $testid;
                    $attempt->answers = json_encode(array_fill(1 , (count($test->questions)), ''));
                    $attempt->save();

                    //If the user has a grade for this test already, dont make another.
                    if(!Grade::where('user_id', '=', Auth::user()->id)->where('assignment_id', '=', $assignmentid)->exists())
                    {
                        $grade = new Grade;
                        $grade->user_id = Auth::user()->id;
                        $grade->assignment_id = $assignmentid;
                        $grade->graded = 0;
                        $grade->percentage = 0;
                        $grade->save();
                    }

                    $question = Question::find($this->latestQuestion($testid));
                }
                else
                {
                    $attempt = Attempt::find(Auth::user()->latestAttempt($testid));
                    $question = Question::find($this->latestQuestion($testid));
                }

                return view('test.question')
                    ->with('attempt', $attempt)
                    ->with('question', $question);
            }
            else
            {
                foreach($classroom->students as $student)
                {
                    //check if the logged in user is a student of the class
                    if($student->id == $user->id)
                    {
                        if(Attempt::attemptAuth($testid))
                        {
                            $test = Test::with('questions')->find($testid);

                            $attempt = new Attempt;
                            $attempt->user_id = Auth::user()->id;
                            $attempt->test_id = $testid;
                            $attempt->answers = json_encode(array_fill(1 , (count($test->questions)), ''));
                            $attempt->save();

                            //If the user has a grade for this test already, dont make another.
                            if(!Grade::where('user_id', '=', Auth::user()->id)->where('assignment_id', '=', $assignmentid)->exists())
                            {
                                $grade = new Grade;
                                $grade->user_id = Auth::user()->id;
                                $grade->assignment_id = $assignmentid;
                                $grade->graded = 0;
                                $grade->percentage = 0;
                                $grade->save();
                            }

                            $question = Question::find($this->latestQuestion($testid));
                        }
                        else
                        {
                            $attempt = Attempt::find(Auth::user()->latestAttempt($testid));
                            $question = Question::find($this->latestQuestion($testid));
                        }

                        return view('test.question')
                            ->with('attempt', $attempt)
                            ->with('question', $question);
                    }
                }
                return redirect('/hub');
            }
        }
    }

    public function autoGrade($attemptid)
    {
        $testid = Attempt::find($attemptid)->test_id;
        $correctcount = 0;
        $questionNumbers = Question::where('test_id', '=', $testid)->lists('set_id');
        $assignmentid = null;

        foreach($questionNumbers as $question)
        {
            $correctAnswers[$question] = Question::where('test_id', '=', $testid)->where('set_id', '=', $question)->value('answer');
        }

        $answers = json_decode(Attempt::find(Auth::user()->latestAttempt($testid))->answers, true);

        foreach($correctAnswers as $questionNumber => $correct)
        {
            $answer = $answers[$questionNumber];

            if($correct == $answer)
            {
                $correctcount++;
            }
        }

        $score = ($correctcount / count($correctAnswers)) * 100;

        $grades = Auth::user()->grades()->get();
        foreach($grades as $grade)
        {
            $assignment = Assignment::find($grade->assignment_id);
            if($testid == $assignment->test_id)
            {
                $assignmentid = $assignment->id;
            }
        }

        $grade1 = Grade::where('user_id', '=', Auth::user()->id)->where('assignment_id', '=', $assignmentid)->first();
        $grade1->graded = 1;
        $grade1->percentage = round($score, 2, PHP_ROUND_HALF_EVEN);
        $grade1->save();

        return redirect('/gradebook');
    }

    public function skipQuestion($attemptid, $questionid)
    {
        $attempt = Attempt::find($attemptid);
        $answers = json_decode($attempt->answers, true);
        $setid = Question::find($questionid)->set_id;
        $answers[$setid] = 'SKIP';
        $attempt->answers = json_encode($answers);
        $attempt->save();
        $nextQuestion = $this->latestQuestion($attempt->test_id);

        return redirect('/attempt/' . $attemptid . '/question/' . $nextQuestion);
    }

    public function viewQuestion($attemptid, $questionid)
    {
        if(Auth::user()->id == Attempt::find($attemptid)->user_id)
        {
            $attempt = Attempt::find($attemptid);
            $question = Question::find($questionid);

            return view('test.question')
                ->with('attempt', $attempt)
                ->with('question', $question);
        }
        return redirect('/hub');
    }

    public function answerQuestion(Request $request)
    {
        if(Auth::user()->id == Attempt::find($request->route('attemptid'))->user_id)
        {
            $attempt = Attempt::find($request->route('attemptid'));
            $answers = json_decode($attempt->answers, true);
            $setid = Question::find($request->route('questionid'))->set_id;
            $answers[$setid] = $request->input('answer');
            $attempt->answers = json_encode($answers);
            $attempt->save();
            $nextQuestion = $this->latestQuestion($attempt->test_id);

            if($nextQuestion == 'completed')
            {
                return view('test.completed')
                    ->with('attempt', $attempt);
            }

            return redirect('/attempt/' . $request->route('attemptid') . '/question/' . $nextQuestion);
        }
        return redirect('/hub');
    }

    public function latestQuestion($id)
    {
        $attempts = Auth::user()->attempts()->where('test_id', '=', $id)->lists('answers');
        $test = Test::find($id);
        if(count($attempts) == 0)
        {
            return $test->questions()->where('set_id', '=', 1)->value('id');
        }
        $answered = false;
        $completed = false;

        //Check for any unanswered questions which would result in a incomplete attempt, then go to that question
        foreach($attempts as $attempt)
        {
            $arrayAttempt = json_decode($attempt);
            foreach($arrayAttempt as $questionNumber => $answer)
            {
                if($answer == '')
                {
                    $setid = $questionNumber;
                    $answered = false;
                    break;
                }
                else
                {
                    $answered = true;
                }
            }
        }

        //if all questions where answered, check for skipped ones
        if($answered)
        {
            foreach($attempts as $attempt)
            {
                $arrayAttempt = json_decode($attempt);
                foreach($arrayAttempt as $questionNumber => $answer)
                {
                    if($answer == 'SKIP')
                    {
                        $setid = $questionNumber;
                        $completed = false;
                        break;
                    }
                    else
                    {
                        $completed = true;
                    }
                }
            }
        }

        if($completed)
        {
            return 'completed';
        }

        return $test->questions()->where('set_id', '=', intval($setid))->value('id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
