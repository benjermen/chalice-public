var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix
        // site styles (site dependency styles should be compiled in the scss file.)
        .sass('chalice.scss') // ADD ANIMATE CSS

        // site dependency scripts
        .scripts([
            'jquery/dist/jquery.js',
            'tether/dist/js/tether.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'typeahead.js/dist/typeahead.bundle.min.js',
            'socket.io-client/socket.io.js'
        ], 'public/js/deps.js', 'vendor/bower_components')

        // site scripts
        .scripts([
            'chalice.js',
            'index.js',
            'typeahead.js'
        ], 'public/js/chalice.js')

        // site dependency fonts
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts');
});


// ADD ANIMATE CSS