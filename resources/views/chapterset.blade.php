@extends('layouts.app')

@section('body')
    <div class="container">

        <div class="list-group">
            <div class="list-group-item">
                <h1 class="list-group-item-heading text-center p-t-1">{{ $chapterset->name }}</h1>
                <br>
                <br>

                <p class="list-group-item-text m-b-1">{{ $chapterset->description }}</p>
            </div>
        </div>
        <br>
        <div class="row heading-center">
            <h2>Chapters</h2>
        </div>
        <div class="list-group">
            @if(count($chapterset->chapters) == 0)
                <div class="list-group">
                    <div class="list-group-item">
                        <h4 class="list-group-item-heading text-center m-b-1">No Chapters.</h4>
                        <p class="list-group-item-text">No chapters in this chapter set yet!</p>
                    </div>
                </div>
            @endif
            @foreach($chapterset->chapters as $chapter)
                <a href="/lesson/{{ $chapter->id }}" class="list-group-item">
                    <h4 class="list-group-item-heading text-center m-b-1">{{ $chapter->name }}</h4>

                    <p class="list-group-item-text">{{ $chapter->description }}</p>
                </a>
            @endforeach
        </div>

    </div>
@stop