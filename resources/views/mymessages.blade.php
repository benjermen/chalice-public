@extends ('layouts.app')

@section('body')
    <div class="col-md-6">
        <div class="row">
            <h2>Notifications</h2>
        </div>
        @if(count(Auth::user()->notifications()->get()) == 0)
            <div class="col-sm-12 blank-panel">
                <h5>No Notifications Presently.</h5>
            </div>
        @endif
        @foreach(Auth::user()->notifications()->get() as $notification)
            <div class="row m-b-1">
                <ul class="list-unstyled" id="notif-list">
                    <li>
                        <a class="content" href="{{ $notification->extension }}">
                            <div class="notification-item">
                                @if(!is_null($notification->from_user_id))
                                    <div class="col-sm-2">
                                        <a href="/user/{{ $notification->from_user_id }}">
                                            <img src="/images/user/{{$notification->from_user_id }}" alt=""
                                                 class="img-circle -align-center">
                                        </a>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <p class="item-info">{{ $notification->body }}</p>

                                    @if($notification->purpose == 1)
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <form action="/acceptfriendrequest/{{ $notification->from_user_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-success" value="Accept">
                                                </form>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <form action="/declinefriendrequest/{{ $notification->from_user_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-danger" value="Decline">
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    @if($notification->purpose == 11)
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <form action="/acceptclassinvite/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-success" value="Accept">
                                                </form>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <form action="/declineclassinvite/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-danger" value="Decline">
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    @if($notification->purpose == 12)
                                        <div class="row">
                                            <div class="col-md-6 text-center">
                                                <form action="/acceptclassrequest/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-success" value="Accept">
                                                </form>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <form action="/declineclassrequest/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="submit" class="btn btn-danger" value="Decline">
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        @endforeach
    </div>
    <div class="col-md-6 messages">
        <div class="row">
            <h2>Conversations</h2>
        </div>
        @if(count(Auth::user()->conversations()->get()) == 0)
            <div class="col-sm-12 blank-panel">
                <h5>No Conversations Presently.</h5>
            </div>
        @endif
        @foreach(Auth::user()->conversations()->get() as $conversation)
            <div class="row m-b-1">
                <div class="col-sm-11">
                    <a href="/conversation/{{ $conversation->id }}" class="hover-none">
                        <div class="conversation row">
                            <div class="col-sm-2">
                                <img src="/images/user/{{ $conversation->messages()->first()->from_user_id }}" alt=""
                                     class="img-circle">
                            </div>
                            <div class="col-sm-4">
                                <h5>{{ App\User::find($conversation->messages()->first()->from_user_id)->fullName() }}</h5>
                            </div>
                            <div class="col-sm-6">
                                <p>{{ $conversation->messages()->latest()->first()->body }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-1">
                    <form action="/conversation/delete/{{ $conversation->id }}" method="post">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-times-circle"></i>
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@stop