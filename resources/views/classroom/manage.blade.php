@extends('layouts.hub')

@section('body')
    <div class="row header-tile" style="background:url('/images/classroom/{{ $classroom->id }}/header') no-repeat 0 0;">
        <div class="m-a-0 row wrapper-classname">
            <div class="col-lg-10 col-md-8">
                <h2>{{ $classroom->name }}</h2>
            </div>
            <div class="col-lg-2 col-md-4">
                <form action="/classroom/updateheader/{{ $classroom->id }}" id="header-form" method="post"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <span class="btn btn-outline-secondary btn-file btn-sm">
                        Header Image <input type="file" name="image" id="header-image-input">
                    </span>
                </form>

                @if(strlen($errors->headerimg->first('image')) > 0)
                    <div class="alert alert-danger fade in">
                        {{ $errors->headerimg->first('image') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="m-a-0 row wrapper-classdescrip">
            <h4>{{ $classroom->description }}</h4>
        </div>
    </div>

    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Active Chapters</h2>
        </div>
        <div class="list-group">
            <a href="/create/chapter">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Add Chapters.</p>
                </div>
            </a>
            <br>
            @if(count($classroom->chapters) > 0)
                @foreach($classroom->chapters as $chapter)
                    <a href="/chapter/{{ $chapter->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $chapter->name }}</h4>
                            <p class="list-group-item-text">{{ $chapter->description }}</p>
                            <a href="/chapter/{{ $chapter->id }}/delete" class="fa fa-times form-btn-stack"></a>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Chapters.</h4>

                    <p class="list-group-item-text">Add some content to the class!</p>
                </div>
            @endif
        </div>
    </div>

    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Assignments</h2>
        </div>
        <div class="list-group">
            <a href="/create/assignment">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Add Assignments.</p>
                </div>
            </a>
            <br>

            @if(count($classroom->assignments) > 0)

                @foreach($classroom->assignments as $assignment)
                    <a href="/assignment/{{ $assignment->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $assignment->name }}</h4>
                            <p class="list-group-item-text">{{ $assignment->description }}</p>
                            <a href="/assignment/{{ $assignment->id }}/delete" class="fa fa-times form-btn-stack"></a>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Assignments.</h4>

                    <p class="list-group-item-text">Start challenging your students!</p>
                </div>
            @endif
        </div>

    </div>

    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Students</h2>
        </div>
        <div class="list-group">
            <a href="/classroom/{{ $classroom->id }}/addstudent">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Add Students.</p>
                </div>
            </a>
            <br>
            @if(count($classroom->students) > 0)
                @foreach($classroom->students as $student)
                    <div class="col-sm-11">
                        <a href="/user/{{ $student->id }}">
                            {{ $student->fullName() }}
                        </a>
                    </div>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Students.</h4>

                    <p class="list-group-item-text">Invite some hungry minds!</p>
                </div>
            @endif
        </div>
    </div>
@stop