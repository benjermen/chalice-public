@extends('layouts.app')

@section('body')
    @if(count($friends) == 0)
        <div class="alert alert-info">You have no Friends!</div>
        @endif

    @foreach($friends as $friend)
        <div class="row content-row">
            <div class="col-md-8">
                {{ $friend->fullName() }}
            </div>
            <div class="col-md-4">
                <form action="/removefriend/{{ $friend->id }}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger" value="Remove Friend">
                </form>
            </div>
        </div>
    @endforeach
@stop