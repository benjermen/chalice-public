@extends('layouts.app')

@section('body')
    <div class="col-md-6">
        <h2>Create a Test</h2>
        <br>

        <form action="/create/test" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="test-name">Name</label>
                <input type="text" class="form-control" id="test-name" name="name" placeholder="Chapter 1 Test..." value="{{ Request::old('name') }}">
            </div>

            @if(strlen($errors->testErrors->first('name')) > 0)
                <div class="alert alert-danger">
                    {{ $errors->testErrors->first('name') }}
                </div>
            @endif

            <div class="form-group">
                <label for="descrip">Description</label>
                <textarea name="description" id="descrip" cols="30" rows="10" class="form-control">{{ Request::old('description') }}</textarea>
            </div>

            @if(strlen($errors->testErrors->first('description')) > 0)
                <div class="alert alert-danger">
                    {{ $errors->testErrors->first('description') }}
                </div>
            @endif

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Create Test">
            </div>
        </form>
    </div>
@stop