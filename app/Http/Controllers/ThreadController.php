<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Thread;
use Auth;
use App\Forum;
use App\User;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $forum = Forum::find($id);

        return view('forums.threadcreate')
            ->with('forum', $forum);

    }

    public function make()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->input('body');
        $rules =
            [
                'title' => 'required|max:300',
                'body' => 'required|max:3000'
            ];
        $validate = \Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return redirect('/createthread/' . $request->route('id'))
                ->withErrors($validate->messages(), 'thread')
                ->withInput($request->all());
        }
        $thread = new Thread;
        $thread->user_id = Auth::user()->id;
        $thread->forum_id = $request->route('id');
        $thread->title = $request->input('title');
        $thread->body = $request->input('body');
        $thread->save();

        return redirect('/forum/' . $request->route('id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thread = Thread::with('posts')->find($id);
        $user = User::find($thread->user_id);

        return view('forums.thread')
            ->with('thread', $thread)
            ->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($threadid)
    {
        $thread = Thread::find(+$threadid);

        //dd($thread);

        // null checks here.

        if (Auth::user()->id == $thread->user_id) {
            $thread->delete();
        }

        return redirect('/forums');
    }
}
