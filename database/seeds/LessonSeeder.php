<?php

use Illuminate\Database\Seeder;
use App\Lesson as Lesson;
use Carbon\Carbon;
use App\Chapter as Chapter;
use App\Comment;
use App\User;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->generateLessons();
    }

    public function generateLessons()
    {

        $chapterids = Chapter::lists('id')->all();
        $userids = User::lists('id')->all();

    	//Initialize
    	$faker = Faker\Factory::create();

    	foreach($chapterids as $chapterid)
    	{
    		for($i = 0; $i < 3; $i++)
            {
                $lesson = new Lesson;
                $lesson->chapter_id = $chapterid;
                $lesson->description = $faker->text;
                $lesson->name = $faker->word;
                $lesson->has_comments = $faker->boolean();
                $lesson->video_url = 'https://www.youtube.com/embed/27dR_sLaM74';
                $lesson->created_at = Carbon::now('America/Detroit');
                $lesson->updated_at = Carbon::now('America/Detroit');
                $lesson->save();

                for($c = 0; $c < 5; $c++)
                {
                    $comment = new Comment;
                    $comment->lesson_id = $lesson->id;
                    $comment->creator_id = $faker->randomElement($userids);
                    $comment->body = $faker->sentence();
                    $comment->likes = $faker->numberBetween(0,10);
                    $comment->dislikes = $faker->numberBetween(0, 7);
                    $comment->updated_at = Carbon::now('America/Detroit');
                    $comment->save();
                }
            }
    	}
    }
}
