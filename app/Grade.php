<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classroom as Classroom;

class Grade extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assignment()
    {
        return $this->belongsTo('App\Assignment');
    }

    public function assignmentName()
    {
        return Assignment::find($this->assignment_id)->name;
    }

    public function teacherName()
    {
        $class = Classroom::find(Assignment::find($this->assignment_id)->classroom_id);
        $name = User::find($class->teacher_id)->fullName();

        return $name;
    }

    public function letterGrade()
    {
        if($this->percentage <= 100 && $this->percentage >= 93.50)
        {
            return 'A';
        }
        elseif($this->percentage >= 89.50)
        {
            return 'A-';
        }
        elseif($this->percentage >= 86.50)
        {
            return 'B+';
        }
        elseif($this->percentage >= 82.50)
        {
            return 'B';
        }
        elseif($this->percentage >= 79.50)
        {
            return 'B-';
        }
        elseif($this->percentage >= 76.50)
        {
            return 'C+';
        }
        elseif($this->percentage >= 72.50)
        {
            return 'C';
        }
        elseif($this->percentage >= 69.50)
        {
            return 'C-';
        }
        elseif($this->percentage >= 66.50)
        {
            return 'D+';
        }
        elseif($this->percentage >= 62.50)
        {
            return 'D';
        }
        elseif($this->percentage >= 59.50)
        {
            return 'D-';
        }
        elseif($this->percentage < 59.50)
        {
            return 'E';
        }
    }
}
