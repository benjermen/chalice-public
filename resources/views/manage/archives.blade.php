@extends('layouts.app')

@section('body')

    <div class="row heading-center">
        <h2>Archives</h2>
    </div>

    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Classrooms</h2>
        </div>
        <div class="list-group">
            <a href="/create/classroom">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Create Classroom.</p>
                </div>
            </a>
            <br>
            @if(count($classrooms) > 0)
                @foreach($classrooms as $classroom)
                    <a href="/classroom/{{ $classroom->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $classroom->name }}</h4>
                            <p class="list-group-item-text">{{ $classroom->description }}</p>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Classrooms.</h4>

                    <p class="list-group-item-text">You have no classrooms to teach.</p>
                </div>
            @endif
        </div>
    </div>
    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Chapter Sets</h2>
        </div>
        <div class="list-group">
            <a href="/create/chapterset">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Create Chapter Set.</p>
                </div>
            </a>
            <br>
            @if(count($chaptersets) > 0)
                @foreach($chaptersets as $chapterset)
                    <a href="/chaptersets/{{ $chapterset->id }}">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $chapterset->name }}</h4>
                            <p class="list-group-item-text">{{ $chapterset->description }}</p>

                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Chapter Sets.</h4>

                    <p class="list-group-item-text">You have no chapter sets.</p>
                </div>
            @endif
        </div>
    </div>
    <div class="hub-column col-md-4">
        <div class="row heading-center">
            <h2>Tests</h2>
        </div>
        <div class="list-group">
            <a href="/create/test">
                <div class="list-group-item text-center">
                    <i class="plus list-group-item-heading fa fa-plus gray"></i>

                    <p class="list-group-item-text">Create Test.</p>
                </div>
            </a>
            <br>
            @if(count($tests) > 0)
                @foreach($tests as $test)
                    <a href="/test/{{ $test->id }}/edit/addquestion">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $test->name }}</h4>
                            <p class="list-group-item-text">{{ $test->description }}</p>

                        </div>
                    </a>
                @endforeach
            @else
                <div class="list-group-item">
                    <h4 class="list-group-item-heading">No Classrooms.</h4>

                    <p class="list-group-item-text">You have no classrooms to teach.</p>
                </div>
            @endif
        </div>
    </div>
@stop

