<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Attempt extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function attemptAuth($id)
    {
        $attempts = Auth::user()->attempts()->where('test_id', '=', $id)->lists('answers')->toArray();
        if(count($attempts) == 0)
        {
            return true;
        }

        foreach($attempts as $attempt)
        {
            $arrayAttempt = json_decode($attempt);
            foreach($arrayAttempt as $questionNumber => $answer)
            {
                if($answer == '')
                {
                    $complete = false;
                    break;
                }
                else
                {
                    $complete = true;
                }
            }
        }

        return $complete;
    }

    public static function grade($attemptid)
    {
        $testid = Attempt::find($attemptid)->test_id;
        $correctcount = 0;
        $questionNumbers = Question::where('test_id', '=', $testid)->lists('set_id');

        foreach($questionNumbers as $question)
        {
            $correctAnswers[$question] = Question::where('set_id', '=', $question)->value('answer');
        }

        $answers = json_decode(Attempt::find($attemptid)->answers, true);

        foreach($correctAnswers as $questionNumber => $correct)
        {
            $answer = $answers[$questionNumber];

            if($correct == $answer)
            {
                $correctcount++;
            }
        }

        $score = ($correctcount / count($correctAnswers)) * 100;

        return round($score, 2, PHP_ROUND_HALF_EVEN);
    }
}
