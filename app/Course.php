<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public function subjects()
    {
    	return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('course_id', 'user_id');
    }
}
