<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User as User;
use App\Test;
use App\Attempt;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($b = 0; $b < 20; $b++)
        {
            factory(App\Question::class)->create(['test_id' => 1, 'set_id' => ($b + 1)]);
        }

        $this->generateAdmin();
        $this->generate();
    }

    /**
     * Generate the app admin.
     */
    protected function generateAdmin()
    {
        $time = Carbon::now('America/Detroit');

        $user = factory(App\User::class)->create([
            'email' => 'admin@chalice.us',
            'password' => bcrypt('password'),
            'first_name' => 'Chalice',
            'last_name' => 'Admin',
            'privilege' => 5,
            'socket' => $this->generateRandString(16),
            'created_at' => $time,
            'updated_at' => $time,
        ]);

        $user->attempts()->save(factory(App\Attempt::class)->make(['test_id' => 1,]));
        $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 1, 'percentage' => Attempt::grade(1),]));
        $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 2,]));
        $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 3,]));
    }

    public function generateRandString($length)
    {
        $randString = '';
        $chars = '0123456789ab(cdef&ghijklm.no*$pqrstu)vwxyzAB$CDEF!G@HIJ$KLMNOPQ^RSTUVWXYZ';
        $charsLength = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $randString .= $chars[rand(0, $charsLength - 1)];
        }
        return $randString;
    }

    public function generate()
    {
        $GLOBALS['classFactory'] = factory(App\Classroom::class)->make(['teacher_id' => 1, 'name' => 'Emerging Tech']);
        $this->command->info('Building Classroom');

        $assignment = factory(App\Assignment::class)->create([
            'classroom_id' => 1,
            'name' => 'Chapter 1 Test',
            'description' => 'Test for chapter 1 in Emerging Tech',
            'test_id' => 1,
        ]);

        $assignment->test()->save(factory(App\Test::class)->make(['creator_id' => 1, 'name' => 'Emerging Tech Chapter 1 Test']));

        factory(App\Assignment::class)->create([
            'classroom_id' => 1,
            'name' => 'Chapter 1 Homework p.62 1-10',
            'description' => 'Homework for 5/29/16',
            'file_name' => 'chap1hw.pdf',
        ]);
        factory(App\Assignment::class)->create([
            'classroom_id' => 1,
            'name' => 'Chapter 2 Homework p.94 16-28',
            'description' => 'Homework for 6/8/16',
            'file_name' => 'chap2hw.docx',
        ]);
        factory(App\Chapter::class)->create([
            'course_id' => null,
            'classroom_id' => 1,
            'name' => 'Chapter 1',
            'description' => 'C# Basic I/O',
        ]);
        factory(App\Chapter::class)->create([
            'course_id' => null,
            'classroom_id' => 1,
            'name' => 'Chapter 2',
            'description' => 'C# Programming Decision Structures',
        ]);
        factory(App\Chapter::class)->create([
            'course_id' => null,
            'classroom_id' => 1,
            'name' => 'Chapter 3',
            'description' => 'C# Programming Loops',
        ]);

        $this->command->info('Building User Data and Relationships...');

        factory(App\User::class, 15)->create()->each(function ($user) {
            $user->classes()->save($GLOBALS['classFactory']);
            $user->attempts()->save(factory(App\Attempt::class)->make(['test_id' => 1,]));
            $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 1, 'percentage' => Attempt::grade(1),]));
            $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 2,]));
            $user->grades()->save(factory(App\Grade::class)->make(['assignment_id' => 3,]));
        });
    }
}
