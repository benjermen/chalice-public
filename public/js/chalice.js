(function($) {

    $(".btn-slidein").click(function() {
        $(".fixed-sidebar").toggleClass("expanded");
        $(".app").toggleClass("expanded");
    });

})(jQuery);


$(document).ready(function(){

    //This is the slide function for the flash success and error messages

        $("#flash-alert").alert();
        $("#flash-alert").fadeTo(3000, 500).slideUp(500, function(){
            $("#flash-alert").alert('close');
    });



    //This is the typeahead suggestion for add a student to a class

    var users = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/queries/users/'
    });

    // initialize the bloodhound suggestion engine
    users.clearPrefetchCache();
    users.initialize();

    // instantiate the typeahead UI
    $('#tt-id .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 2
    },
    {
        source: users.ttAdapter(),
        name: 'users',
        displayKey: 'inviteid',
        templates: {
            empty: [
                '<div class="no-results tt-suggestion">No results.</div>'
            ].join('\n'),

            suggestion: function (data) {
                return '<div class="user-search-result"><img class="img-circle" src="/images/user/' + data.id + '"/><p class="suggestion-fullname">' + data.name + '</p></div>';
            }
        }
    });

    // instantiate the typeahead UI
    $('#tt-general-search .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        },
        {
            source: users.ttAdapter(),
            name: 'users',
            displayKey: 'name',
            templates: {
                empty: [
                    '<div class="no-results tt-suggestion">No results.</div>'
                ].join('\n'),

                suggestion: function (data) {
                    return '<div class="user-search-result"><a class="btn-block" href="/user/' + data.id + '"><img class="img-circle" src="/images/user/' + data.id + '"/><p class="suggestion-fullname">' + data.name + '</p></a></div>';
                }
            }
        });

    //This is the typeahead suggestion for requesting classroom access

    var classes = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/queries/classes/'
    });

    // initialize the bloodhound suggestion engine
    classes.clearPrefetchCache();
    classes.initialize();

    // instantiate the typeahead UI
    $('#tt-class-request .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 2
        },
        {
            source: classes.ttAdapter(),
            name: 'classes',
            displayKey: 'joincode',
            templates: {
                empty: [
                    ''
                ].join('\n'),

                suggestion: function (data) {
                    return '<div class="class-search-result"><img class="img-circle" src="/images/user/' + data.teacher_id + '"/><p class="suggestion-fullname">' + data.name + '</p></div>';
                }
            }
        });
});
var socket = io('http://localhost:3000');
var notifCount = $('.badge .notifcount');
var notifCountWrapper = $('#notif-count');

function hideAns(checkbox)
{
    $('#ans').toggleClass("hidden");
    $('#c-d').toggleClass("hidden");
    $('.t-f').toggleClass("col-xs-3").toggleClass("col-xs-6");
}

function hasFromUser(notification)
{
    if(notification.from_user_id != null)
    {
        return '<a href="/user/' + notification.from_user_id + '"><img src="/images/user/' + notification.from_user_id + '" alt="" class="notif-img"> </a>';
    }
}

function isFriendRequest(notification)
{
    if(notification.purpose == 1)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptfriendrequest/' + notification.from_user_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declinefriendrequest/' + notification.from_user_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function isClassInvite(notification)
{
    if(notification.purpose == 11)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptclassinvite/' + notification.from_user_id + '/' + notification.opt_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declineclassinvite/' +  notification.from_user_id + '/' + notification.opt_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function isClassRequest(notification)
{
    if(notification.purpose == 12)
    {
        return '<div class="row"> <div class="col-md-6 text-center"> <form action="/acceptclassrequest/' + notification.from_user_id + '/' + notification.opt_id + '" method="post">' + csrf() + '<input type="submit" class="btn btn-success" value="Accept"></form></div><div class="col-md-6 text-center"> <form action="/declineclassrequest/' +  notification.from_user_id + '/' + notification.opt_id + ' "method="post">' + csrf() + '<input type="submit" class="btn btn-danger" value="Decline"></form></div></div>';
    }
}

function csrf()
{
    return '<input type="hidden" name="_token" value="' + $('meta[name="_token"]').attr('content') + '">';
}

function user_channel()
{
    return $('meta[name="channel"]').attr('content');
}


socket.on("notifAction:App\\Events\\NotificationCreated:" + user_channel(), function (notification) {

    if(!notifCount.val())
    {
        notifCountWrapper.html('<span class="badge notifcount">1</span>');
    }
    else
    {
        var count = parseInt(notifCount.text());
        count++;
        notifCount.text(count);
    }

    notifCount.addClass('animated bounce');
    setTimeout(function() {
        notifCount.removeClass('animated bounce');
    }, 1200);

    if(notification.purpose == 1)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isFriendRequest(notification) + '</div> </a> </li>');
    }

    if(notification.purpose == 11)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isClassInvite(notification) + '</div> </a> </li>');
    }

    if(notification.purpose == 12)
    {
        $("#no-notif").hide();
        $("#notif-list").append('<li><a class="content" href="#"><div class="notification-item">' + hasFromUser(notification) + '<p class="item-info">' + notification.body + '</p>' + isClassRequest(notification) + '</div> </a> </li>');
    }
});

