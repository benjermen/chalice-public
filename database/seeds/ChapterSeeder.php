<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Chapter as Chapter;

class ChapterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->chapters();
    }

    public function chapters()
    {

    	//Initialize faker
    	$faker = Faker\Factory::create();

    	$courseChapters = [
	    	'Algebra Basics' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Algebra I' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Algebra II' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Pre-Calculus' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Calculus' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Physics' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Chemistry' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Biology' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'American History' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'World History' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Microeconomics' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Macroeconomics' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Computer Programming with C++' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Computer Programming with Java' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Web Development' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'Computer Science' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'SAT' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
	    	'MCAT' => [
	    		'Chapter 1',
	    		'Chapter 2',
	    		'Chapter 3',
	    		'Chapter 4',
	    	],
    	];

    	$lessonids = range(1,50);

    	foreach($courseChapters as $courseName => $course)
    	{
    		$courseid = DB::table('courses')->where('name', '=', $courseName)->value('id');

    		foreach($course as $chapter)
    		{
    			$lessons = $faker->randomElements($lessonids, $count = 4);
    			
    			$chapter1 = new Chapter;
	    		$chapter1->course_id = $courseid;
	    		$chapter1->description = $faker->text;
	    		$chapter1->name = $chapter;
	    		$chapter1->created_at = Carbon::now('America/Detroit');
	        	$chapter1->updated_at = Carbon::now('America/Detroit');
	        	$chapter1->save();
    		}
    	}
    }
}
