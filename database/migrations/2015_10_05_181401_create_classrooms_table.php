<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function(Blueprint $table){
            $table->increments('id');
            $table->integer('teacher_id')->unsigned();
            $table->string('header_image');
            $table->string('name');
            $table->string('description');
            $table->string('join_code');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('classrooms', function($table) {
            $table->foreign('teacher_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('classrooms');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
