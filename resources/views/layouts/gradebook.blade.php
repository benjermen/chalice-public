<!DOCTYPE html>
<html lang="en" class="full-height">
<head>
    @section('meta')
        @include('partials.meta')
    @show
</head>

<body class="app full-height">
<div class="container-fluid full-height">
    <div class="row topbar-nav">

        @section('topbar')
            @include('modules.topbar')
        @show

    </div>
    <div class="row full-height">
        <div class="p-l-0 full-height fixed-sidebar">
            @section('nav')
                @include('modules.nav')
            @show
        </div>
        <div class="fixed-content">
            <div class="full-height">
                <ul class="col-md-2 grade-sidebar">
                    @if(Auth::user()->teacher())
                        <a class="grade-link">
                            <li class="grade-link-separator">
                                --  Classrooms --
                            </li>
                        </a>
                        @if(isset($classrooms))
                            @foreach($classrooms as $classroom)
                                @if(!isset($counter))
                                    <?php $counter = 1; ?>
                                @endif
                                <a href="/gradebook/classroom/{{ Auth::user()->id }}/{{ $classroom->id }}" class="grade-link">
                                    <li class="grade-link-element @if($counter % 2 == 0) darkened @endif">
                                        {{ $classroom->name }}
                                    </li>
                                </a>
                                <?php $counter++; ?>
                            @endforeach
                        @endif
                        <a class="grade-link">
                            <li class="grade-link-separator">
                                --  Classes --
                            </li>
                        </a>
                        @foreach($classes as $class)
                            <a href="/gradebook/class/{{ Auth::user()->id }}/{{ $class->id }}" class="grade-link">
                                <li class="grade-link-element">
                                    {{ $class->name }}
                                </li>
                            </a>
                        @endforeach
                    @else
                        <a class="grade-link">
                            <li class="grade-link-separator">
                                --  Classes --
                            </li>
                        </a>
                        @foreach($classes as $class)
                            <a href="/gradebook/class/{{ Auth::user()->id }}/{{ $class->id }}" class="grade-link">
                                <li class="grade-link-element">
                                    {{ $class->name }}
                                </li>
                            </a>
                        @endforeach
                    @endif
                </ul>

                <div class="col-md-10 grade-display">
                    @yield('body')
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer')
    @include('partials.footer')
@show
</body>
</html>