@extends('layouts.app')

@section('body')
    <div class="iframe-wrap">
        <iframe class="video-iframe" src="{{ $lesson->video_url }}" frameborder="0" allowfullscreen></iframe>
    </div>

    <div class="row m-a-0 lesson-name">
        <div class="col-md-8 col-md-offset-1">
            <h1>{{ $lesson->name }}</h1>
        </div>
        <div class="col-md-2">
            <div class="col-md-6">
                <i class="fa fa-thumbs-o-up fa-2x"></i>
            </div>
            <div class="col-md-6">
                <i class="fa fa-thumbs-o-down fa-2x"></i>
            </div>
        </div>
    </div>
    <div class="container m-t-3">
        <form action="/addcomment/{{ $lesson->id }}" method="post">
            {{ csrf_field() }}
            <div class="comment-add-wrapper">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <div class="comment-add">
                    <h5>Add Comment.</h5>
                    <div class="form-group">
                        <textarea name="comment" id="commentbody" cols="15" rows="5" class="form-control"></textarea>
                    </div>
                    <input type="submit" class="btn btn-primary-outline" value="Post Comment">
                </div>
            </div>
        </form>

        @foreach($lesson->comments as $comment)
            <div class="comment-wrapper">
                <div class="comment-content">
                    <img src="/images/user/{{ $comment->creator_id }}" alt="" class="comment-img">

                    <a href="/user/{{ $comment->creator_id }}" class="comment-fullname-link"><h4
                                class="comment-fullname">{{ \App\User::find($comment->creator_id)->fullName() }}</h4>
                    </a>

                    <div class="small-icon-wrapper">
                        <button class="small-icon" name="{{ $comment->id }}" id="commentLike"><i
                                    class="fa fa-thumbs-o-up"></i></button>
                        <p class="badge">{{ $comment->likes }}</p>
                    </div>
                    <div class="small-icon-wrapper">
                        <button class="small-icon" name="{{ $comment->id }}" id="commentLike"><i
                                    class="fa fa-thumbs-o-down"></i></button>
                        <p class="badge">{{ $comment->dislikes }}</p>
                    </div>

                    <p class="lightgray-faint">{{ \Carbon\Carbon::createFromTimestamp($comment->updated_at->getTimestamp())->format('M j, Y g:i a') }}</p>

                    <p class="comment-body">{{ $comment->body }}</p>
                </div>
            </div>
        @endforeach
    </div>
@stop