@extends('layouts.app')

@section('body')
    <div class="col-md-6 col-lg-4">
        <form action="/classroom/{{ $classroom->id }}/addstudent" method="post">
            {{ csrf_field() }}
            <div class="form-group" id="tt-id">
                <label for="name">Student Invite</label>
                    <input type="text" placeholder="Name or Id Code" class="form-control typeahead" name="name">
            </div>

            <input type="submit" class="btn btn-primary" value="Invite Student">
        </form>

        @if(Session::has('error') || Session::has('success'))
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
        @endif

        <div class="form-group m-t-1">
            <div class="alert alert-info">
                Your students may also request for access to this with the class code: <strong>{{ $classroom->join_code }}</strong>
            </div>
        </div>

        <div class="form-group m-t-1">
            <div class="alert alert-info">
                If the search function is not locating your student, you may ask the student for their Invite ID, which is located on the student's settings page, and manually enter the 6 digit code.
            </div>
        </div>
    </div>
@stop