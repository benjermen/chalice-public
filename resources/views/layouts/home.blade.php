<!DOCTYPE html>
<html lang="en">
    <head>
        @section('meta')
            @include('partials/meta')
        @show
    </head>

    <body class="home">
        @section('header')
            @include('partials/header')
        @show

        @yield('body')

        @section('footer')
            @include('partials/footer')
        @show
    </body>
</html>
