<div class="sidebar-nav full-height">
    <nav>
        <ul class="list-unstyled">
            @if(Auth::check())
                <li>
                    <a href="/learn">
                        <span class="fa-icon fa-fw fa fa-graduation-cap"></span>
                        <span class="icon-class">Learn</span>
                    </a>
                </li>
                    @if(Auth::user()->teacher())
                        <li>
                            <a href="/manage">
                                <span class="fa-icon fa-fw fa fa-pencil"></span>
                                <span class="icon-class">Manage</span>
                            </a>
                        </li>
                    @endif
                <li>
                    <a href="/user/{{ Auth::user()->id }}">
                        <span class="fa-icon fa-fw fa fa-user"></span>
                        <span class="icon-class">My Profile</span>
                    </a>
                </li>
                <li>
                    <a href="/gradebook">
                        <span class="fa-icon fa-fw fa fa-book"></span>
                        <span class="icon-class">Gradebook</span>
                    </a>
                </li>
                <li>
                    <a href="/messages">
                        <span class="fa-icon fa-fw fa fa-envelope"></span>
                        <span class="icon-class">Messages</span>
                    </a>
                </li>
                <li>
                    <a href="/friends">
                        <span class="fa-icon fa-fw fa fa-users"></span>
                        <span class="icon-class">Friends</span>
                    </a>
                </li>
                @if(Auth::user()->privilege == 2 || Auth::user()->privilege == 5)
                    <li>
                        <a href="/modpanel">
                            <span class="fa-icon fa-fw fa fa-lock"></span>
                            <span class="icon-class">Moderator Panel</span>
                        </a>
                    </li>
                @endif
                @if(Auth::user()->privilege == 3 || Auth::user()->privilege == 5)
                    <li>
                        <a href="/adminpanel">
                            <span class="fa-icon fa-fw fa fa-lock"></span>
                            <span class="icon-class">Admin Panel</span>
                        </a>
                    </li>
                @endif
                <li>
                    <a href="/forums">
                        <span class="fa-icon fa-fw fa fa-list"></span>
                        <span class="icon-class">Forums</span>
                    </a>
                </li>
                <li>
                    <a href="/settings">
                        <span class="fa-icon fa-fw fa fa-cogs"></span>
                        <span class="icon-class">Settings</span>
                    </a>
                </li>
                @if(Auth::user()->teacher())
                    <li>
                        <a href="/create">
                            <span class="fa-icon fa-fw fa fa-plus-square-o"></span>
                            <span class="icon-class">Create</span>
                        </a>
                    </li>
                @endif
            @endif

            @if(!Auth::check())
                <li>
                    <a href="/auth/register">
                        <span class="fa-icon fa-fw fa fa-sign-in"></span>
                        <span class="icon-class">Register</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
</div>
