<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subject as Subject;
use App\Course as Course;
use App\Chapter as Chapter;
use App\Lesson as Lesson;
use App\User as User;
use App\Classroom;
use Auth;
use App\Comment;
use Validator;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subject, $course, $chapter, $lesson)
    {

        $subjectid = Subject::where('name', '=', $subject)->value('id');
        $courseid = Course::where('subject_id', '=', $subjectid)->where('name', '=', $course)->value('id');
        $chapterid = Chapter::where('course_id', '=', $courseid)->where('name', '=', $chapter)->value('id');
        $lessonModel = Lesson::with('comments')->where('chapter_id', '=', $chapterid)->where('name', '=', $lesson)->first();

        if (Auth::check() && !User::courseCheck($courseid)) {
            $user = Auth::user();

            $user->addCourse($courseid);
        }

        return view('lesson')
            ->with('lesson', $lessonModel)
            ->with('subject_name', $subject)
            ->with('course_name', $course)
            ->with('chapter_name', $chapter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::extend('yturl', function ($attribute, $value, $parameters) {

            $pattern = '/^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/';
            if (preg_match($pattern, $value))
            {
                return true;
            }
            return false;
        });

        $rules = [
            'lesson_name' => 'required|max:30',
            'videourl' => 'required|yturl',
            'lesson_description' => 'required|max:200',
        ];

        $messages = [
            'lesson_name.required' => 'The name field is required',
            'lesson_name.max' => 'The name field must be less than 30 characters.',
            'lesson_description.max' => 'The description field must be less than 200 characters.',
            'lesson_description.required' => 'The description field is required.',
            'videourl.required' => 'The video URL field is required.',
            'videourl.yturl' => 'The video URL field must contain a YouTube URL.',
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('/create/chapter')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'lessoncreate');
        }


        $lesson = new Lesson;

        if ($request->has('chapter-select')) {
            $lesson->chapter_id = $request->input('chapter-select');
        } else {
            $lesson->chapter_id = null;
        }

        if ($request->input('comments') == 'on') {
            $lesson->has_comments = 1;
        } else {
            $lesson->has_comments = 0;
        }

        $videourl = $request->input('videourl');
        $videourl = explode('=', $videourl);
        $videourl = 'https://www.youtube.com/embed/' . $videourl[1];

        $lesson->name = $request->input('lesson_name');
        $lesson->video_url = $videourl;
        $lesson->description = $request->input('lesson_description');
        $lesson->save();

        $success = 'Lesson Created!';

        return redirect('/create/chapter')
            ->with('lesson-success', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapter = Chapter::find(Lesson::find($id)->chapter_id);

        if (Auth::guest()) {
            return redirect()->back();
        }

        if ($chapter->classroom_id == null) {
            $lesson = Lesson::with('comments')->find($id);

            return view('lesson')->with('lesson', $lesson);
        } else {
            $classroom = Classroom::find($chapter->classroom_id);

            if (Auth::user()->id == $classroom->teacher_id) {
                $lesson = Lesson::with('comments')->find($id);

                return view('lesson')->with('lesson', $lesson);
            }

            foreach ($classroom->students as $student) {
                if ($student->id == Auth::user()->id) {
                    $lesson = Lesson::with('comments')->find($id);

                    return view('lesson')->with('lesson', $lesson);
                }
            }
        }

        return redirect('hub');
    }

    public function addComment(Request $request)
    {
        if(Auth::guest())
        {
            return redirect()->back()->with('error', 'You must be logged in to post a comment.');
        }

        $comment = new Comment;
        $comment->lesson_id = $request->route('lessonid');
        $comment->creator_id = Auth::user()->id;
        $comment->body = $request->input('comment');
        $comment->likes = 0;
        $comment->dislikes = 0;
        $comment->updated_at = Carbon::now('America/Detroit');
        $comment->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
