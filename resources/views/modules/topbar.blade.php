<div class="col-md-5 col-xs-5">
    <button class="btn-slidein fa fa-bars fa-2x"></button>

    <div class="form-group hidden-md-down" id="tt-general-search">
        <input type="text" placeholder="Search for friends..." class="form-control typeahead" name="name">
    </div>
</div>

<div class="col-md-2 col-xs-2">
    <h2><a href="/hub">CHALICE</a></h2>
</div>

<div class="col-md-5 col-xs-5">
    <div class="dropdown-notifs">
        <a id="dLabel" role="button" data-toggle="dropdown" data-target="#">
            <i class="fa fa-bell-o dd-icon"></i>
            @if(Auth::check())
                <div id="notif-count">
                    @if(count(Auth::user()->notifications()->get()) != 0)
                        <span class="badge">
                        {{ count(Auth::user()->notifications()->get()) }}
                    </span>
                    @endif
                </div>
            @endif
        </a>

        <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">

            <div class="notifications-wrapper">
                @if(Auth::check())
                    @if(count(Auth::user()->notifications()->get()) == 0)
                        <div id="no-notif">
                            <a class="content" href="#">
                                <div class="notification-item">
                                    <h4 class="item-title"></h4>

                                    <p class="item-info">No Notifications Presently!</p>
                                </div>
                            </a>
                        </div>
                    @endif
                    <ul class="list-unstyled" id="notif-list">
                        @foreach(Auth::user()->notifications()->get() as $notification)
                            <li>
                                <a class="content" href="{{ $notification->extension }}">
                                    <div class="notification-item">
                                        @if(!is_null($notification->from_user_id))
                                            <a href="/user/{{ $notification->from_user_id }}">
                                                <img src="/images/user/{{$notification->from_user_id }}" alt=""
                                                     class="notif-img">
                                            </a>
                                        @endif
                                        <p class="item-info">{{ $notification->body }}</p>
                                        @if($notification->purpose == 1)
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <form action="/acceptfriendrequest/{{ $notification->from_user_id }}"
                                                          method="post">
                                                        {{ csrf_field() }}
                                                        <input type="submit" class="btn btn-success" value="Accept">
                                                    </form>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <form action="/declinefriendrequest/{{ $notification->from_user_id }}"
                                                          method="post">
                                                        {{ csrf_field() }}
                                                        <input type="submit" class="btn btn-danger" value="Decline">
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                        @if($notification->purpose == 11)
                                            <div class="row">
                                                <div class="col-md-6 text-center">
                                                    <form action="/acceptclassinvite/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                          method="post">
                                                        {{ csrf_field() }}
                                                        <input type="submit" class="btn btn-success" value="Accept">
                                                    </form>
                                                </div>
                                                <div class="col-md-6 text-center">
                                                    <form action="/declineclassinvite/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                          method="post">
                                                        {{ csrf_field() }}
                                                        <input type="submit" class="btn btn-danger" value="Decline">
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                            @if($notification->purpose == 12)
                                                <div class="row">
                                                    <div class="col-md-6 text-center">
                                                        <form action="/acceptclassrequest/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                              method="post">
                                                            {{ csrf_field() }}
                                                            <input type="submit" class="btn btn-success" value="Accept">
                                                        </form>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                        <form action="/declineclassrequest/{{ $notification->from_user_id }}/{{ $notification->opt_id }}"
                                                              method="post">
                                                            {{ csrf_field() }}
                                                            <input type="submit" class="btn btn-danger" value="Decline">
                                                        </form>
                                                    </div>
                                                </div>
                                            @endif
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @elseif(Auth::guest())
                    <a class="content" href="#">
                        <div class="notification-item">
                            <h4 class="item-title"></h4>

                            <p class="item-info">You need a Chalice account to get notifications!</p>
                        </div>
                    </a>
                @endif
            </div>
            <li class="divider"></li>
            <div class="notification-footer">
                <a href="/messages">
                    <h4 class="menu-title">View all</h4>
                </a>
            </div>
        </ul>
    </div>

    @if(Auth::check())
        <div class="vert-top">
            <img src="/images/user/{{ Auth::user()->id }}" alt="" class="dropdown-toggle w-34"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

            <ul class="dropdown-menu" role="menu">
                <a href="/user/{{ Auth::user()->id }}"><li>Profile</li></a>
                <a href="/settings"><li>Settings</li></a>
                <li role="separator" class="divider"></li>
                <a href="/auth/logout"><li>Log out</li></a>
            </ul>
        </div>
    @endif
</div>





