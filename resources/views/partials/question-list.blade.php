<div class="list-group">
    <a href="#" class="list-group-item active">Questions</a>
    @if(count($test->questions) > 0)
        @foreach($test->questions as $question)
            <div class="list-group-item">
                <a href="/test/{{ $test->id }}/edit/question/{{ $question->id }}" class="break-word gooby">{{ $question->set_id }}. <span class="p-l-1">{{ $question->question }}</span></a>
                <a href="/test/{{ $test->id }}/delete/question/{{ $question->id }}" class="fa fa-times form-btn-stack"></a>
            </div>
        @endforeach
    @else
        <a href="#" class="list-group-item">No questions in this test!</a>
    @endif
</div>