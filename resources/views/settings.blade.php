@extends('layouts.app')

@section('body')
    <div class="page-content">
        <p><strong>Your Settings</strong></p>

        <form action="/settings/changeEmail" method="post">
            {{--contains the Change email process--}}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="user-emailReset" class="control-label">Change Email</label>
                        <input class="form-control m-b-1" maxlength="255"
                               placeholder="Please enter new email here" type="email" size="50"
                               name="newEmail" value="{{old('newEmail') }}">
                        @if(strlen($errors->settings->first('newEmail')) > 0)
                            <div class="alert alert-danger fade in">
                                {{ $errors->settings->first('newEmail') }}
                            </div>
                        @endif
                        @if(Session::has('emailSuccess'))
                            <div class="alert alert-success">
                                {{ Session::get('emailSuccess') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            {{--reset email button--}}
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block" value="Reset Email">
                    </div>
                </div>
            </div>
        </form>
        <form action="/settings/resetPassword" method="post">
            {{--Contains the password reset properties--}}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="user-passwordReset" class="control-label">Reset Password</label>
                        <input class="form-control" maxlength="16" id="user-originalPassword "
                               placeholder="Please enter your old password here" type="password" size="50"
                               name="oldPassword">
                        @if(strlen($errors->settings->first('oldPassword')) > 0)
                            <div class="alert alert-danger fade in">
                                {{ $errors->settings->first('oldPassword') }}
                            </div>
                        @endif
                        <br>
                        <input class="form-control" maxlength="16" id="user-passwordNew "
                               placeholder="Please enter your new password here" type="password" size="50"
                               name="newPassword">

                        <input class="form-control" maxlength="16" id="user-passwordConfirm "
                               placeholder="Please re-enter your new password" type="password" size="50"
                               name="passwordConfirmation">
                        @if(strlen($errors->settings->first('newPassword')) > 0)
                            <div class="alert alert-danger fade in">
                                {{ $errors->settings->first('newPassword') }}
                            </div>
                        @endif

                        @if(Session::has('passwordError'))
                            <div class="alert alert-danger">
                                {{ Session::get('passwordError') }}
                            </div>
                        @endif

                        @if(Session::has('passwordSuccess'))
                            <div class="alert alert-success">
                                {{ Session::get('passwordSuccess') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block" value="Reset Password">
                    </div>
                </div>
            </div>
        </form>
        <form action="/settings/image" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <label for="img-upload">Profile Image</label>

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <input type="file" name="image" class="m-b-1" id="img-upload">
                    @if(strlen($errors->settings->first('image')) > 0)
                        <div class="alert alert-danger fade in">
                            {{ $errors->settings->first('image') }}
                        </div>
                    @endif
                    @if(Session::has('imgSuccess'))
                        <div class="alert alert-success">
                            {{ Session::get('imgSuccess') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block" value="Upload Profile Image">
                    </div>
                </div>
            </div>

            <div class="row">
                <p>Invite ID: <strong>{{ Auth::user()->invite_id }} </strong></p>
            </div>
        </form>
    </div>
@stop

