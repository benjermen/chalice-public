@extends('layouts.app')

@section('body')
    <h2>Create Classroom</h2>
    <form action="/create/classroom" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="class-name" class="control-label">Name</label>
            <input type="text" class="form-control" id="class-name" name="class_name" placeholder="English 101..." value="{{ Request::old('class_name') }}">
        </div>
        @if(strlen($errors->classroomcreate->first('class_name')) > 0)
            <div class="alert alert-danger">
                {{ $errors->classroomcreate->first('class_name') }}
            </div>
        @endif
        <div class="form-group">
            <label for="class-description" class="control-label">Description</label>
            <textarea name="class_description" id="class-description" cols="30" rows="10" class="form-control" placeholder="Describe your class...">{{ Request::old('class_description') }}</textarea>
        </div>
        @if(strlen($errors->classroomcreate->first('class_description')) > 0)
            <div class="alert alert-danger">
                {{ $errors->classroomcreate->first('class_description') }}
            </div>
        @endif
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Create Classroom!">
        </div>
        @if(Session::has('classroom-success'))
            <div class="alert alert-success">
                {{ Session::get('classroom-success') }}
            </div>
        @endif
    </form>
@stop