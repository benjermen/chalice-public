<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('from_user_id')->nullable()->unsigned();
            $table->string('body');
            $table->integer('purpose')->unsigned();
            $table->string('extension')->nullable();
            $table->integer('opt_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('notifications', function($table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('from_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
