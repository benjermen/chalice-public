<?php

namespace App\Http\Controllers;

use App\Classroom;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Chapter;
use Auth;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classrooms = \Auth::user()->classrooms()->get();

        return view('create.chapter')
            ->with('classrooms', $classrooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'chapter-name' => 'required|max:35',
            'chapter-description' => 'required|max:80',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/create/chapter')
                ->withInput($request->all())
                ->withErrors($validator->messages(), 'chapterErrors');
        }

        $chapter = new Chapter;
        $chapter->name = $request->input('chapter-name');
        $chapter->description = $request->input('chapter-description');
        $chapter->classroom_id = $request->input('classroom-select');
        $chapter->save();

        return redirect('/create/chapter')->with('chapterSuccess', 'Chapter created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::guest())
        {
            return redirect()->back();
        }

        if (Chapter::find($id)->classroom_id == null)
        {
            $chapter = Chapter::with('lessons')->find($id);

            return view('chapter')->with('chapter', $chapter);
        }
        else
        {
            $classroom = Classroom::find(Chapter::find($id)->classroom_id);

            if(Auth::user()->id == $classroom->teacher_id)
            {
                $chapter = Chapter::with('lessons')->find($id);

                return view('chapter')->with('chapter', $chapter);
            }

            foreach ($classroom->students as $student)
            {
                if ($student->id == Auth::user()->id)
                {
                    $chapter = Chapter::with('lessons')->find($id);

                    return view('chapter')->with('chapter', $chapter);
                }
            }
        }

        return redirect('hub');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacherid = Chapter::find($id)->classroom()->value('teacher_id');

        if(Auth::user()->id == $teacherid)
        {
            $chapter = Chapter::find($id);

            $chapter->delete();

            return redirect()->back();
        }

        return redirect()->back();
    }
}
