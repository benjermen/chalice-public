@extends('layouts.app')

@section('body')
        <div class="container col-md-4">
            <h1>Submit a Thread</h1>
            <form action="/createthread/{{ $forum->id }}" method="post">
                {{ csrf_field() }}
                <div class=" form-group col-sm-6">
                <input class="m-a-1" type="text" name="title" value="{{ Request::old('title') }}" placeholder="Title">
                <textarea class="m-a-1" name="body" rows="4" cols="50">{{ Request::old('body') }}</textarea>
                <input type="submit" class="m-a-1 btn btn-primary" value="Submit Your Thread">
                </div>
            </form>
        </div>
        @if(strlen($errors->thread->first()) > 0)
            <div class="alert alert-danger fade in">
                {{ $errors->thread->first() }}
            </div>
        @endif

@stop