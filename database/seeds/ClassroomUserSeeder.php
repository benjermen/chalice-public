<?php

use Illuminate\Database\Seeder;
use App\Classroom as Classroom;
use App\User as User;

class ClassroomUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->generateClassroomStudents();
    }

    public function generateClassroomStudents()
    {
        $classroomids = Classroom::lists('id')->toArray();
        $userids = User::where('privilege', 0)->lists('id')->toArray();

        foreach($classroomids as $classroom)
        {
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
            DB::statement('insert into classroom_user values (?,?)', [$classroom, Faker\Factory::create()->randomElement($userids)]);
        }
    }
}
